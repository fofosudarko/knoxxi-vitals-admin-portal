import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Nav } from 'react-bootstrap';
import { BiHomeAlt2 } from 'react-icons/bi';
import Link from 'next/link';
import { useDashboardStore } from 'src/stores/dashboard';
import { MdBusinessCenter, MdOutlineHealthAndSafety } from 'react-icons/md';
import { LuUsers } from 'react-icons/lu';
import Image from 'next/image';

import { useRoutes, useDeviceDimensions } from 'src/hooks';
import { APP_DASHBOARD_LOGO2 } from 'src/config';

import { AppContainer } from 'src/components/lib';

function AppMain({ children, appUser }) {
  return (
    <AppContainer NavArea={<AppMainNav appUser={appUser} />} disablePaddingTop>
      {children}
    </AppContainer>
  );
}

export function AppMainNav() {
  const { isLargeDevice } = useDeviceDimensions();
  const { mainNav, setMainNav } = useDashboardStore((state) => state);

  const { homeRoute } = useRoutes().useHomeRoute();
  const { userDataListRoute } = useRoutes().useUserDataListRoute();
  const { partnersRoute } = useRoutes().usePartnersRoute();
  const { healthRoute } = useRoutes().useHealthRoute();

  const handleSelect = useCallback(
    (selectedKey) => {
      if (selectedKey === homeRoute) {
        setMainNav(homeRoute);
      } else if (selectedKey === userDataListRoute) {
        setMainNav(userDataListRoute);
      } else if (selectedKey === partnersRoute) {
        setMainNav(partnersRoute);
      } else if (selectedKey === healthRoute) {
        setMainNav(healthRoute);
      }
    },
    [healthRoute, userDataListRoute, homeRoute, partnersRoute, setMainNav]
  );

  const navItems = useMemo(() => {
    return [
      {
        route: homeRoute,
        Icon: <BiHomeAlt2 size={20} />,
        label: 'Home',
        eventKey: homeRoute,
        active: mainNav === homeRoute,
        disabled: false,
      },
      {
        route: userDataListRoute,
        Icon: <LuUsers size={20} />,
        label: 'Users',
        eventKey: userDataListRoute,
        active: mainNav === userDataListRoute,
        disabled: false,
      },
      {
        route: partnersRoute,
        Icon: <MdBusinessCenter size={20} />,
        label: 'Partners',
        eventKey: partnersRoute,
        active: mainNav === partnersRoute,
        disabled: false,
      },
      {
        route: healthRoute,
        Icon: <MdOutlineHealthAndSafety size={20} />,
        label: 'Health',
        eventKey: healthRoute,
        active: mainNav === healthRoute,
        disabled: false,
      },
    ];
  }, [healthRoute, userDataListRoute, homeRoute, mainNav, partnersRoute]);

  return (
    <Nav
      onSelect={handleSelect}
      className="flex-column my-1 vh-100"
      activeKey="home"
    >
      <Nav.Item>
        <Link href={homeRoute} as={homeRoute}>
          <div className="pt-3 pb-5 ps-3">
            <Image
              src={APP_DASHBOARD_LOGO2}
              alt="Knoxxi Logo"
              height={isLargeDevice ? '63' : '63'}
              width={isLargeDevice ? '170' : '170'}
            />
          </div>
        </Link>
      </Nav.Item>
      {navItems.map((item, index) => (
        <Nav.Item key={index}>
          <Nav.Link
            eventKey={item.eventKey}
            as="div"
            disabled={item.disabled}
            style={{ cursor: item.disabled ? 'not-allowed' : 'pointer' }}
            className={`${
              item.active ? 'app-nav-active-link' : 'app-nav-inactive-link'
            }`}
          >
            <Link
              href={item.route}
              as={item.route}
              className="text-decoration-none"
            >
              <>
                <div
                  className={`${
                    item.active
                      ? 'app-nav-active-text'
                      : 'app-nav-inactive-text'
                  }`}
                >
                  <div className="app-nav-text text-lowercase">
                    <div className="d-inline-block">{item.Icon}</div>
                    <span className="mx-2">{item.label}</span>
                  </div>
                </div>
              </>
            </Link>
          </Nav.Link>
        </Nav.Item>
      ))}
    </Nav>
  );
}

AppMain.propTypes = {
  children: PropTypes.node,
  appUser: PropTypes.object,
};
AppMain.defaultProps = {
  children: null,
  appUser: null,
};

export default AppMain;
