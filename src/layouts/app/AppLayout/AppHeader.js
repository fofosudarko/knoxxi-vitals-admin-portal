import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Nav,
  Navbar,
  Container,
  Offcanvas,
  NavDropdown,
} from 'react-bootstrap';
import Link from 'next/link';
import Image from 'next/image';

import { APP_DASHBOARD_LOGO } from 'src/config';
import { useSignOut, useRoutes, useDeviceDimensions } from 'src/hooks';

import { NavButton, SignOut, NewHOC } from 'src/components/lib';

function AppHeader({
  appUser,
  SideNav: _SideNav,
  hideNavbarBrand,
  disableNavbarContainerFluid,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const { useHomeRoute } = useRoutes();
  const { homeRoute } = useHomeRoute();

  const SideNav = NewHOC(_SideNav);

  return (
    <Navbar expand="lg" className="app-header" sticky="top">
      <Container
        fluid={!disableNavbarContainerFluid}
        style={{ width: isLargeDevice ? '100%' : '100%' }}
        className={`${
          isLargeDevice ? 'd-flex justify-content-around' : undefined
        }`}
      >
        <div>
          <MainNav>
            <SideNav appUser={appUser} />
          </MainNav>
          {!hideNavbarBrand ? (
            <Navbar.Brand className="mx-2">
              <Link href={homeRoute} as={homeRoute}>
                <>
                  <Image
                    src={APP_DASHBOARD_LOGO}
                    alt="Knoxxi Logo"
                    height={isLargeDevice ? '28' : '25'}
                    width={isLargeDevice ? '85' : '80'}
                  />
                </>
              </Link>
            </Navbar.Brand>
          ) : (
            <div></div>
          )}
        </div>
        {!isLargeDevice ? (
          <>
            <Navbar.Toggle as={NavButton}></Navbar.Toggle>
            <Navbar.Collapse>
              <AppHeaderNav appUser={appUser} />
            </Navbar.Collapse>
          </>
        ) : (
          <div>
            <Navbar.Collapse>
              <AppHeaderNav appUser={appUser} />
            </Navbar.Collapse>
          </div>
        )}
      </Container>
    </Navbar>
  );
}

export function AppHeaderNav({ appUser }) {
  const handleSignOut = useSignOut();

  const { account = {} } = appUser ?? {};

  let profileItem = null;

  const handleSelect = useCallback(
    (selectedKey) => {
      if (selectedKey === 'sign-out') {
        handleSignOut();
      }
    },
    [handleSignOut]
  );

  return (
    <div className="w-auto my-2 my-md-0 d-flex justify-content-start justify-content-lg-end">
      <Nav onSelect={handleSelect}>
        <NavDropdown
          title={`Welcome, ${
            account.customerName ? account.customerName : ''
          }!`}
          align={{ sm: 'start' }}
          id="nav-dropdown"
          menuRole="menu"
          className="pe-2 rounded shadown-sm text-white"
        >
          {profileItem ? (
            <>
              <NavDropdown.Divider />
              <NavDropdown.Item
                eventKey={profileItem.eventKey}
                as="div"
                style={{
                  cursor: profileItem.disabled ? 'not-allowed' : 'pointer',
                }}
                disabled={profileItem.disabled}
              >
                <Link
                  href={profileItem.route}
                  as={profileItem.route}
                  className="text-decoration-none"
                >
                  <>
                    <div
                      className={`text-decoration-none ${
                        profileItem.active ? 'text-secondary' : 'text-black'
                      } fw-normal`}
                    >
                      <div className="d-inline-block fs-5">
                        {profileItem.Icon}
                      </div>
                      <span className="fs-6 mx-2">{profileItem.label}</span>
                    </div>
                  </>
                </Link>
              </NavDropdown.Item>
            </>
          ) : null}

          <NavDropdown.Divider />
          <NavDropdown.Item eventKey="sign-out">
            <SignOut />
          </NavDropdown.Item>
        </NavDropdown>
      </Nav>
    </div>
  );
}

function MainNav({ children }) {
  const [showMainNav, setShowMainNav] = useState(false);

  const handleShowMainNav = useCallback(() => {
    setShowMainNav((state) => !state);
  }, []);

  return (
    <div className="d-block d-lg-none">
      <NavButton onClick={handleShowMainNav} variant="white" />
      <Offcanvas show={showMainNav} onHide={handleShowMainNav}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title></Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body className="app-nav-area-bg">
          <div onClick={handleShowMainNav}>{children}</div>
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  );
}

AppHeader.propTypes = {
  appUser: PropTypes.object,
  SideNav: PropTypes.func,
  hideNavbarBrand: PropTypes.bool,
  disableNavbarContainerFluid: PropTypes.bool,
};
AppHeader.defaultProps = {
  appUser: null,
  SideNav: null,
  hideNavbarBrand: false,
  disableNavbarContainerFluid: false,
};
AppHeaderNav.propTypes = {
  appUser: PropTypes.object,
};
AppHeaderNav.defaultProps = {
  appUser: null,
};
MainNav.propTypes = {
  children: PropTypes.node,
};
MainNav.defaultProps = {
  children: null,
};

export default AppHeader;
