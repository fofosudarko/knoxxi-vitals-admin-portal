import PropTypes from 'prop-types';

import AuthMain from './AuthMain';

function AuthLayout({ children }) {
  return (
    <div className="mx-0 w-100 d-block">
      <AuthMain>{children}</AuthMain>
    </div>
  );
}

AuthLayout.propTypes = {
  children: PropTypes.node,
};

export function getAuthLayout(page) {
  return <AuthLayout>{page}</AuthLayout>;
}

export default AuthLayout;
