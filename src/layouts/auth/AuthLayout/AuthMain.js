import PropTypes from 'prop-types';
import { Container } from 'react-bootstrap';
import Image from 'next/image';

import { APP_LOGO } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

function AuthMain({ children }) {
  const { isSmallDevice, isMediumDevice, isLargeDevice } =
    useDeviceDimensions();

  return (
    <Container fluid className="main-site">
      <div className="d-flex flex-column align-items-center justify-content-center vh-100">
        <div
          className="bg-white p-3 rounded shadow"
          style={{
            width: isSmallDevice
              ? '100%'
              : isMediumDevice
              ? '75%'
              : isLargeDevice
              ? '20%'
              : '100%',
          }}
        >
          <div className="my-5 text-center">
            <Image
              src={APP_LOGO}
              alt="Knoxxi Logo"
              width={isLargeDevice ? 200 : 100}
              height={isLargeDevice ? 70 : 35}
            />
          </div>
          <div>{children}</div>
        </div>
      </div>
    </Container>
  );
}

AuthMain.propTypes = {
  children: PropTypes.node,
};
AuthMain.defaultProps = {
  children: null,
};

export default AuthMain;
