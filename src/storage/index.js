// storage.js

import * as keys from './keys';

function saveReduxState(state) {
  localStorage.setItem(
    keys.KNOXXI_RADIO_ADMIN_PORTAL_REDUX_STATE,
    JSON.stringify(state)
  );
}

function getReduxState() {
  const serializedState = localStorage.getItem(
    keys.KNOXXI_RADIO_ADMIN_PORTAL_REDUX_STATE
  );

  return serializedState === undefined || serializedState === null
    ? undefined
    : JSON.parse(serializedState);
}

export { saveReduxState, getReduxState };
