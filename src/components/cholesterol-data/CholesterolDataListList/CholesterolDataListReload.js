import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetCholesterolDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function CholesterolDataListReload({ useTooltip }) {
  const _handleResetCholesterolDataList = useResetCholesterolDataList();

  const handleResetCholesterolDataList = useCallback(() => {
    _handleResetCholesterolDataList();
  }, [_handleResetCholesterolDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetCholesterolDataList}
      text="Reload cholesterol data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

CholesterolDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
CholesterolDataListReload.defaultProps = {
  useTooltip: false,
};

export default CholesterolDataListReload;
