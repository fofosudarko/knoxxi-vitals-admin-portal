import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  CholesterolDataTableItem,
  CholesterolDataGridItem,
} from './CholesterolDataItem';

export function CholesterolDataListList({
  cholesterolDataList,
  loadingText,
  appUser,
  onLoadMore,
  CholesterolDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!cholesterolDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(CholesterolDataListListEmpty);

  return cholesterolDataList.length ? (
    <div>
      {isLargeDevice ? (
        <CholesterolDataListListTable
          cholesterolDataList={cholesterolDataList}
          appUser={appUser}
        />
      ) : (
        <CholesterolDataListListGrid
          cholesterolDataList={cholesterolDataList}
          appUser={appUser}
        />
      )}

      {cholesterolDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more cholesterol data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function CholesterolDataListListGrid({ cholesterolDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {cholesterolDataList.map((item) => (
          <Col key={item.id}>
            <CholesterolDataGridItem cholesterolData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function CholesterolDataListListTable({ cholesterolDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Total cholesterol reading(mmol/L)
            </th>
            <th className="list-table-head-cell">
              Severity(Total cholesterol)
            </th>
            <th className="list-table-head-cell">
              Interpretation(Total cholesterol)
            </th>
            <th className="list-table-head-cell">
              HDL cholesterol reading(mmol/L)
            </th>
            <th className="list-table-head-cell">Severity(HDL cholesterol)</th>
            <th className="list-table-head-cell">
              Interpretation(HDL cholesterol)
            </th>
            <th className="list-table-head-cell">
              LDL cholesterol reading(mmol/L)
            </th>
            <th className="list-table-head-cell">Severity(LDL cholesterol)</th>
            <th className="list-table-head-cell">
              Interpretation(LDL cholesterol)
            </th>
            <th className="list-table-head-cell">
              Triglycerides cholesterol reading(mmol/L)
            </th>
            <th className="list-table-head-cell">
              Severity(Triglycerides cholesterol)
            </th>
            <th className="list-table-head-cell">
              Interpretation(Triglycerides cholesterol)
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {cholesterolDataList.map((item, index) => (
            <CholesterolDataTableItem
              cholesterolData={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function CholesterolDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no cholesterol data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

CholesterolDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  cholesterolDataList: PropTypes.array,
  CholesterolDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
CholesterolDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading cholesterol data list...',
  onLoadMore: undefined,
  cholesterolDataList: null,
  CholesterolDataListListEmpty: null,
  isDisabled: false,
};
CholesterolDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  cholesterolDataList: PropTypes.array,
};
CholesterolDataListListGrid.defaultProps = {
  appUser: null,
  cholesterolDataList: null,
};
CholesterolDataListListTable.propTypes = {
  appUser: PropTypes.object,
  cholesterolDataList: PropTypes.array,
};
CholesterolDataListListTable.defaultProps = {
  appUser: null,
  cholesterolDataList: null,
};
CholesterolDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
CholesterolDataListListEmpty.defaultProps = {
  appUser: null,
};
