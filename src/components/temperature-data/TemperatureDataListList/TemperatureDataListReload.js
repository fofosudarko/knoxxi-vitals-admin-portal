import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetTemperatureDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function TemperatureDataListReload({ useTooltip }) {
  const _handleResetTemperatureDataList = useResetTemperatureDataList();

  const handleResetTemperatureDataList = useCallback(() => {
    _handleResetTemperatureDataList();
  }, [_handleResetTemperatureDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetTemperatureDataList}
      text="Reload temperature data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

TemperatureDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
TemperatureDataListReload.defaultProps = {
  useTooltip: false,
};

export default TemperatureDataListReload;
