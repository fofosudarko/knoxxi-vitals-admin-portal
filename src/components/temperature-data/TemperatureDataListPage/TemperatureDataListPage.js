import PropTypes from 'prop-types';

import { TemperatureDataListListView } from '../TemperatureDataListList/TemperatureDataListList';

function TemperatureDataListPage({ appUser }) {
  const query = null;
  return (
    <div>
      <div className="page-title">Temperature data</div>
      <div className="page-content">
        <TemperatureDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

TemperatureDataListPage.propTypes = {
  appUser: PropTypes.object,
};
TemperatureDataListPage.defaultProps = {
  appUser: null,
};

export default TemperatureDataListPage;
