import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveUserData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function UserDataRemove({ userData, appUser, useTooltip }) {
  const {
    handleRemoveUserData: _handleRemoveUserData,
    error,
    setError,
    userDataRemoved,
    setUserDataRemoved,
  } = useRemoveUserData({ appUser });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (userDataRemoved) {
      handleApiResult();
    }
  }, [userDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('User data removed successfully').getAlertNotification()
    );
    setUserDataRemoved(false);
  }, [handleNotification, setUserDataRemoved]);

  const handleRemoveUserData = useCallback(async () => {
    await _handleRemoveUserData(userData);
  }, [_handleRemoveUserData, userData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveUserData}
        variant="white"
        text="Remove user data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

UserDataRemove.propTypes = {
  userData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
UserDataRemove.defaultProps = {
  userData: null,
  appUser: null,
  useTooltip: false,
};

export default UserDataRemove;
