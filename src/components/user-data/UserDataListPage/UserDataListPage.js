import PropTypes from 'prop-types';

import { UserDataListListView } from '../UserDataListList/UserDataListList';

function UserDataListPage({ appUser }) {
  const query = null;
  return (
    <div>
      <div className="page-title">User data</div>
      <div className="page-content">
        <UserDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

UserDataListPage.propTypes = {
  appUser: PropTypes.object,
};
UserDataListPage.defaultProps = {
  appUser: null,
};

export default UserDataListPage;
