import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_CONTENT_NAME_VALIDATOR } from 'src/config/validators';
import { useNotificationHandler } from 'src/hooks';
import { useSelectItems } from 'src/hooks/api';

import { FilterContainer } from 'src/components/lib';
import { UserDataListList, UserDataListListEmpty } from './_UserDataListList';

function UserDataListSearch({ query, onSearch, appUser }) {
  const [{ fullname }, setSearchTerms] = useState({});

  useEffect(() => {
    onSearch && onSearch(fullname);
  }, [onSearch, fullname]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div>
      <FilterContainer>
        <UserDataListSearchInput onSubmit={handleSubmitSearchTerms} />
      </FilterContainer>
      {fullname ? (
        <div className="my-2">
          <UserDataListSearchOutput
            fullname={fullname}
            query={query}
            appUser={appUser}
          />
        </div>
      ) : null}
    </div>
  );
}

function UserDataListSearchInput({ onSubmit }) {
  const userDataListSearchInputSchema = Yup({
    fullname: YUP_CONTENT_NAME_VALIDATOR,
  });

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(userDataListSearchInputSchema),
  });

  const watchedFullname = watch('fullname');

  useEffect(() => {
    const fullname = watchedFullname;
    onSubmit && onSubmit({ fullname });
  }, [onSubmit, watchedFullname]);

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Controller
              name="fullname"
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Search user data list by fullname"
                  {...field}
                  isInvalid={!!errors.fullname}
                />
              )}
            />
            {errors.fullname ? (
              <Form.Control.Feedback type="invalid">
                {errors.fullname.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}

function UserDataListSearchOutput({ fullname, query, appUser }) {
  const {
    error,
    setError,
    userDataList,
    setUserDataList,
    handleListUserDataList,
  } = useSelectItems();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    (async () => {
      await handleListUserDataList({
        ...query,
        fullname,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query, fullname]);

  useEffect(() => {
    return () => {
      setUserDataList(null);
    };
  }, [setUserDataList]);

  return (
    <UserDataListList
      userDataList={userDataList}
      loadingText="Searching user data list..."
      UserDataListListEmpty={UserDataListListEmpty}
      isSearch
      appUser={appUser}
    />
  );
}

UserDataListSearch.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
UserDataListSearch.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
UserDataListSearchOutput.propTypes = {
  mobileNumber: PropTypes.string,
  fullname: PropTypes.string,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
UserDataListSearchOutput.defaultProps = {
  mobileNumber: undefined,
  fullname: undefined,
  query: null,
  appUser: null,
};
UserDataListSearchInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
};
UserDataListSearchInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
};

export default UserDataListSearch;
