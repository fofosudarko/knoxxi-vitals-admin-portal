import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetUserDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function UserDataListReload({ useTooltip }) {
  const _handleResetUserDataList = useResetUserDataList();

  const handleResetUserDataList = useCallback(() => {
    _handleResetUserDataList();
  }, [_handleResetUserDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetUserDataList}
      text="Reload user data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

UserDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
UserDataListReload.defaultProps = {
  useTooltip: false,
};

export default UserDataListReload;
