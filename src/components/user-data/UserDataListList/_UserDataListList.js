import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { UserDataTableItem, UserDataGridItem } from './UserDataItem';

export function UserDataListList({
  userDataList,
  loadingText,
  appUser,
  onLoadMore,
  UserDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!userDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(UserDataListListEmpty);

  return userDataList.length ? (
    <div>
      {isLargeDevice ? (
        <UserDataListListTable userDataList={userDataList} appUser={appUser} />
      ) : (
        <UserDataListListGrid userDataList={userDataList} appUser={appUser} />
      )}

      {userDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more user data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function UserDataListListGrid({ userDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {userDataList.map((item) => (
          <Col key={item.id}>
            <UserDataGridItem userData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function UserDataListListTable({ userDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Mobile number</th>
            <th className="list-table-head-cell">Full name</th>
            <th className="list-table-head-cell">Email address</th>
            <th className="list-table-head-cell">Gender</th>
            <th className="list-table-head-cell"></th>
          </tr>
        </thead>
        <tbody>
          {userDataList.map((item, index) => (
            <UserDataTableItem userData={item} appUser={appUser} key={index} />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function UserDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no user data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

UserDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  userDataList: PropTypes.array,
  UserDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
UserDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading user data list...',
  onLoadMore: undefined,
  userDataList: null,
  UserDataListListEmpty: null,
  isDisabled: false,
};
UserDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  userDataList: PropTypes.array,
};
UserDataListListGrid.defaultProps = {
  appUser: null,
  userDataList: null,
};
UserDataListListTable.propTypes = {
  appUser: PropTypes.object,
  userDataList: PropTypes.array,
};
UserDataListListTable.defaultProps = {
  appUser: null,
  userDataList: null,
};
UserDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
UserDataListListEmpty.defaultProps = {
  appUser: null,
};
