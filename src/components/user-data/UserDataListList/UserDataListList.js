import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListUserDataList, useSearchItems } from 'src/hooks/api';

import UserDataListReload from './UserDataListReload';
import { UserDataListList, UserDataListListEmpty } from './_UserDataListList';
import UserDataListSearch from './UserDataListSearch';

function UserDataListListContainer({ appUser, query }) {
  const {
    userDataList,
    error: userDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListUserDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (userDataListError) {
      handleNotification(userDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, userDataListError, setError]);

  const handleLoadMoreUserDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <UserDataListList
      userDataList={userDataList}
      onLoadMore={handleLoadMoreUserDataList}
      loadingText="Loading user data list..."
      appUser={appUser}
      UserDataListListEmpty={UserDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function UserDataListListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <UserDataListReload useTooltip />
      </div>
      <div>
        <UserDataListSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <UserDataListListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

UserDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
UserDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
UserDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  userDataList: PropTypes.array,
  UserDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
UserDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading user data list...',
  onLoadMore: undefined,
  userDataList: null,
  UserDataListListEmpty: null,
  isDisabled: false,
};
UserDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
UserDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default UserDataListList;
