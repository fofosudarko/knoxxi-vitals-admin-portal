import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';

import UserDataRemove from '../UserDataRemove/UserDataRemove';

export function useUserDataDetails(userData) {
  const { mobileNumber, fullname, emailAddress, gender } = userData ?? {};

  return {
    mobileNumber,
    fullname,
    emailAddress,
    gender,
  };
}

export function UserDataGridItem({ userData }) {
  const { mobileNumber, fullname, emailAddress, gender } =
    useUserDataDetails(userData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Mobile number</div>
            <div className="item-subtitle">
              {mobileNumber ? mobileNumber : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Full name</div>
            <div className="item-subtitle">{fullname ? fullname : 'N/A'}</div>
          </Col>
          <Col>
            <div className="item-subtitle">
              <div className="item-title">Email address</div>
              {emailAddress ? emailAddress : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Gender</div>
            <div className="item-subtitle">{gender ? gender : 'N/A'}</div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              <UserDataItemActions userData={userData} />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function UserDataTableItem({ userData }) {
  const { mobileNumber, fullname, emailAddress, gender } =
    useUserDataDetails(userData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {mobileNumber ? mobileNumber : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {fullname ? fullname : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {emailAddress ? emailAddress : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {gender ? gender : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          <UserDataItemActions userData={userData} />
        </div>
      </td>
    </tr>
  );
}

export function UserDataItemActions({ userData }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <UserDataRemove userData={userData} />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

UserDataGridItem.propTypes = {
  userData: PropTypes.object,
};
UserDataGridItem.defaultProps = {
  userData: null,
};
UserDataTableItem.propTypes = {
  userData: PropTypes.object,
};
UserDataTableItem.defaultProps = {
  userData: null,
};
UserDataItemActions.propTypes = {
  userData: PropTypes.object,
};
UserDataItemActions.defaultProps = {
  userData: null,
};
