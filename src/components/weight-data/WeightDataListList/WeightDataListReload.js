import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetWeightDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function WeightDataListReload({ useTooltip }) {
  const _handleResetWeightDataList = useResetWeightDataList();

  const handleResetWeightDataList = useCallback(() => {
    _handleResetWeightDataList();
  }, [_handleResetWeightDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetWeightDataList}
      text="Reload weight data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

WeightDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
WeightDataListReload.defaultProps = {
  useTooltip: false,
};

export default WeightDataListReload;
