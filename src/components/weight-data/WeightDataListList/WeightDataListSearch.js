import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_CONTENT_NAME_VALIDATOR } from 'src/config/validators';
import { useNotificationHandler } from 'src/hooks';
import { useSelectItems } from 'src/hooks/api';

import { FilterContainer } from 'src/components/lib';
import {
  WeightDataListList,
  WeightDataListListEmpty,
} from './_WeightDataListList';

function WeightDataListSearch({ query, onSearch, appUser }) {
  const [{ userDataFullname }, setSearchTerms] = useState({});

  useEffect(() => {
    onSearch && onSearch(userDataFullname);
  }, [onSearch, userDataFullname]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div>
      <FilterContainer>
        <WeightDataListSearchInput onSubmit={handleSubmitSearchTerms} />
      </FilterContainer>
      {userDataFullname ? (
        <div className="my-2">
          <WeightDataListSearchOutput
            userDataFullname={userDataFullname}
            query={query}
            appUser={appUser}
          />
        </div>
      ) : null}
    </div>
  );
}

function WeightDataListSearchInput({ onSubmit }) {
  const weightDataListSearchInputSchema = Yup({
    userDataFullname: YUP_CONTENT_NAME_VALIDATOR,
  });

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(weightDataListSearchInputSchema),
  });

  const watchedUserDataFullname = watch('userDataFullname');

  useEffect(() => {
    const userDataFullname = watchedUserDataFullname;
    onSubmit && onSubmit({ userDataFullname });
  }, [onSubmit, watchedUserDataFullname]);

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Controller
              name="userDataFullname"
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Search weight data list by user fullname"
                  {...field}
                  isInvalid={!!errors.userDataFullname}
                />
              )}
            />
            {errors.userDataFullname ? (
              <Form.Control.Feedback type="invalid">
                {errors.userDataFullname.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}

function WeightDataListSearchOutput({ userDataFullname, query, appUser }) {
  const {
    error,
    setError,
    weightDataList,
    setWeightDataList,
    handleListWeightDataList,
  } = useSelectItems();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    (async () => {
      await handleListWeightDataList({
        ...query,
        userDataFullname,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query, userDataFullname]);

  useEffect(() => {
    return () => {
      setWeightDataList(null);
    };
  }, [setWeightDataList]);

  return (
    <WeightDataListList
      weightDataList={weightDataList}
      loadingText="Searching weight data list..."
      WeightDataListListEmpty={WeightDataListListEmpty}
      isSearch
      appUser={appUser}
    />
  );
}

WeightDataListSearch.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
WeightDataListSearch.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
WeightDataListSearchOutput.propTypes = {
  userDataFullname: PropTypes.string,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
WeightDataListSearchOutput.defaultProps = {
  userDataFullname: undefined,
  query: null,
  appUser: null,
};
WeightDataListSearchInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
};
WeightDataListSearchInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
};

export default WeightDataListSearch;
