import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { WeightDataTableItem, WeightDataGridItem } from './WeightDataItem';

export function WeightDataListList({
  weightDataList,
  loadingText,
  appUser,
  onLoadMore,
  WeightDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!weightDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(WeightDataListListEmpty);

  return weightDataList.length ? (
    <div>
      {isLargeDevice ? (
        <WeightDataListListTable
          weightDataList={weightDataList}
          appUser={appUser}
        />
      ) : (
        <WeightDataListListGrid
          weightDataList={weightDataList}
          appUser={appUser}
        />
      )}

      {weightDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more weight data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function WeightDataListListGrid({ weightDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {weightDataList.map((item) => (
          <Col key={item.id}>
            <WeightDataGridItem weightData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function WeightDataListListTable({ weightDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Weight(kg)</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {weightDataList.map((item, index) => (
            <WeightDataTableItem
              weightData={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function WeightDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no weight data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

WeightDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  weightDataList: PropTypes.array,
  WeightDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
WeightDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading weight data list...',
  onLoadMore: undefined,
  weightDataList: null,
  WeightDataListListEmpty: null,
  isDisabled: false,
};
WeightDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  weightDataList: PropTypes.array,
};
WeightDataListListGrid.defaultProps = {
  appUser: null,
  weightDataList: null,
};
WeightDataListListTable.propTypes = {
  appUser: PropTypes.object,
  weightDataList: PropTypes.array,
};
WeightDataListListTable.defaultProps = {
  appUser: null,
  weightDataList: null,
};
WeightDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
WeightDataListListEmpty.defaultProps = {
  appUser: null,
};
