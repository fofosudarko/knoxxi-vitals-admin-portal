import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListWeightDataList, useSearchItems } from 'src/hooks/api';

import WeightDataListReload from './WeightDataListReload';
import {
  WeightDataListList,
  WeightDataListListEmpty,
} from './_WeightDataListList';
import WeightDataListSearch from './WeightDataListSearch';

function WeightDataListListContainer({ appUser, query }) {
  const {
    weightDataList,
    error: weightDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListWeightDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (weightDataListError) {
      handleNotification(weightDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, weightDataListError, setError]);

  const handleLoadMoreWeightDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <WeightDataListList
      weightDataList={weightDataList}
      onLoadMore={handleLoadMoreWeightDataList}
      loadingText="Loading weight data list..."
      appUser={appUser}
      WeightDataListListEmpty={WeightDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function WeightDataListListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <WeightDataListReload useTooltip />
      </div>
      <div>
        <WeightDataListSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <WeightDataListListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

WeightDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
WeightDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
WeightDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  weightDataList: PropTypes.array,
  WeightDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
WeightDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading weight data list...',
  onLoadMore: undefined,
  weightDataList: null,
  WeightDataListListEmpty: null,
  isDisabled: false,
};
WeightDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
WeightDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default WeightDataListList;
