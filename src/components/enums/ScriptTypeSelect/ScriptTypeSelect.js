import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromScriptType(scriptType = null) {
  if (!scriptType) {
    return '';
  }

  return typeof scriptType === 'object' ? scriptType.name ?? null : scriptType;
}

function getScriptTypeOptionItem(scriptType = null) {
  if (!scriptType) {
    return {};
  }

  const value = getValueFromScriptType(scriptType);

  return {
    value,
    label: humanizeString({ string: value, capitalize: true }),
  };
}

const ScriptTypeSelect = forwardRef(function ScriptTypeSelect(
  {
    name,
    onChange,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    isDisabled,
    placeholder,
    helpText,
    hideLabel,
    label,
    labelClass,
  },
  ref
) {
  const selectProps = {
    name,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    onChange,
    isDisabled,
    placeholder,
    helpText,
    hideLabel,
    label,
    labelClass,
  };
  const { error, setError, scriptTypes, setScriptType, handleListScriptType } =
    useListEnums();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (!scriptTypes?.length) {
      (async () => {
        await handleListScriptType();
      })();
    }
  }, [handleListScriptType, scriptTypes]);

  useEffect(() => {
    return () => {
      setScriptType(null);
    };
  }, [setScriptType]);

  return (
    <ItemSelect
      {...selectProps}
      ref={ref}
      items={scriptTypes}
      getOptionItem={getScriptTypeOptionItem}
      isSync
      isRawValue
    />
  );
});

ScriptTypeSelect.displayName = 'ScriptTypeSelect';

ScriptTypeSelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
};
ScriptTypeSelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
};

export default ScriptTypeSelect;
