import PropTypes from 'prop-types';

import { WaterDataListListView } from '../WaterDataListList/WaterDataListList';

function WaterDataListPage({ appUser }) {
  const query = null;
  return (
    <div>
      <div className="page-title">Water data</div>
      <div className="page-content">
        <WaterDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

WaterDataListPage.propTypes = {
  appUser: PropTypes.object,
};
WaterDataListPage.defaultProps = {
  appUser: null,
};

export default WaterDataListPage;
