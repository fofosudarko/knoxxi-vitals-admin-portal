import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { WaterDataTableItem, WaterDataGridItem } from './WaterDataItem';

export function WaterDataListList({
  waterDataList,
  loadingText,
  appUser,
  onLoadMore,
  WaterDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!waterDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(WaterDataListListEmpty);

  return waterDataList.length ? (
    <div>
      {isLargeDevice ? (
        <WaterDataListListTable
          waterDataList={waterDataList}
          appUser={appUser}
        />
      ) : (
        <WaterDataListListGrid
          waterDataList={waterDataList}
          appUser={appUser}
        />
      )}

      {waterDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more water data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function WaterDataListListGrid({ waterDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {waterDataList.map((item) => (
          <Col key={item.id}>
            <WaterDataGridItem waterData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function WaterDataListListTable({ waterDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Reading(ml)</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {waterDataList.map((item, index) => (
            <WaterDataTableItem
              waterData={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function WaterDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no water data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

WaterDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  waterDataList: PropTypes.array,
  WaterDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
WaterDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading water data list...',
  onLoadMore: undefined,
  waterDataList: null,
  WaterDataListListEmpty: null,
  isDisabled: false,
};
WaterDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  waterDataList: PropTypes.array,
};
WaterDataListListGrid.defaultProps = {
  appUser: null,
  waterDataList: null,
};
WaterDataListListTable.propTypes = {
  appUser: PropTypes.object,
  waterDataList: PropTypes.array,
};
WaterDataListListTable.defaultProps = {
  appUser: null,
  waterDataList: null,
};
WaterDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
WaterDataListListEmpty.defaultProps = {
  appUser: null,
};
