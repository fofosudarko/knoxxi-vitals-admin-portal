import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetWaterDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function WaterDataListReload({ useTooltip }) {
  const _handleResetWaterDataList = useResetWaterDataList();

  const handleResetWaterDataList = useCallback(() => {
    _handleResetWaterDataList();
  }, [_handleResetWaterDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetWaterDataList}
      text="Reload water data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

WaterDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
WaterDataListReload.defaultProps = {
  useTooltip: false,
};

export default WaterDataListReload;
