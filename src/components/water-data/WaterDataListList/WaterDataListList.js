import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListWaterDataList, useSearchItems } from 'src/hooks/api';

import WaterDataListReload from './WaterDataListReload';
import {
  WaterDataListList,
  WaterDataListListEmpty,
} from './_WaterDataListList';
import WaterDataListSearch from './WaterDataListSearch';

function WaterDataListListContainer({ appUser, query }) {
  const {
    waterDataList,
    error: waterDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListWaterDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (waterDataListError) {
      handleNotification(waterDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, waterDataListError, setError]);

  const handleLoadMoreWaterDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <WaterDataListList
      waterDataList={waterDataList}
      onLoadMore={handleLoadMoreWaterDataList}
      loadingText="Loading water data list..."
      appUser={appUser}
      WaterDataListListEmpty={WaterDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function WaterDataListListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <WaterDataListReload useTooltip />
      </div>
      <div>
        <WaterDataListSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <WaterDataListListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

WaterDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
WaterDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
WaterDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  waterDataList: PropTypes.array,
  WaterDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
WaterDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading water data list...',
  onLoadMore: undefined,
  waterDataList: null,
  WaterDataListListEmpty: null,
  isDisabled: false,
};
WaterDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
WaterDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default WaterDataListList;
