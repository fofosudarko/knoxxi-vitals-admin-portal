import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetOxygenDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function OxygenDataListReload({ useTooltip }) {
  const _handleResetOxygenDataList = useResetOxygenDataList();

  const handleResetOxygenDataList = useCallback(() => {
    _handleResetOxygenDataList();
  }, [_handleResetOxygenDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetOxygenDataList}
      text="Reload oxygen data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

OxygenDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
OxygenDataListReload.defaultProps = {
  useTooltip: false,
};

export default OxygenDataListReload;
