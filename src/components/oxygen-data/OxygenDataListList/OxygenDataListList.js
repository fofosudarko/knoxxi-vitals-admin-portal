import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListOxygenDataList, useSearchItems } from 'src/hooks/api';

import OxygenDataListReload from './OxygenDataListReload';
import {
  OxygenDataListList,
  OxygenDataListListEmpty,
} from './_OxygenDataListList';
import OxygenDataListSearch from './OxygenDataListSearch';

function OxygenDataListListContainer({ appUser, query }) {
  const {
    oxygenDataList,
    error: oxygenDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListOxygenDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (oxygenDataListError) {
      handleNotification(oxygenDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, oxygenDataListError, setError]);

  const handleLoadMoreOxygenDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <OxygenDataListList
      oxygenDataList={oxygenDataList}
      onLoadMore={handleLoadMoreOxygenDataList}
      loadingText="Loading oxygen data list..."
      appUser={appUser}
      OxygenDataListListEmpty={OxygenDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function OxygenDataListListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <OxygenDataListReload useTooltip />
      </div>
      <div>
        <OxygenDataListSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <OxygenDataListListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

OxygenDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
OxygenDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
OxygenDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  oxygenDataList: PropTypes.array,
  OxygenDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
OxygenDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading oxygen data list...',
  onLoadMore: undefined,
  oxygenDataList: null,
  OxygenDataListListEmpty: null,
  isDisabled: false,
};
OxygenDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
OxygenDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default OxygenDataListList;
