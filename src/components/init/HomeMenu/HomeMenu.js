import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { MdBusinessCenter, MdOutlineHealthAndSafety } from 'react-icons/md';
import { LuUsers } from 'react-icons/lu';

import { useRoutes } from 'src/hooks';

import { MenuCard } from 'src/components/lib';

function HomeMenu({ appUser }) {
  const homeItems = [
    {
      Icon: <LuUsers size={200} className="text-primary" />,
      label: 'Users',
    },
    {
      Icon: <MdBusinessCenter size={200} className="text-primary" />,
      label: 'Partners',
    },
    {
      Icon: <MdOutlineHealthAndSafety size={200} className="text-primary" />,
      label: 'Health',
    },
  ];

  return (
    <div className="mt-2">
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        {homeItems.map((item, index) => (
          <Col key={index}>
            <HomeMenuItem
              Icon={item.Icon}
              label={item.label}
              appUser={appUser}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
}

export function HomeMenuItem({ label, Icon }) {
  const { useUserDataListRoute, usePartnersRoute, useHealthRoute } =
    useRoutes();
  const { handleUserDataListRoute } = useUserDataListRoute();
  const { handlePartnersRoute } = usePartnersRoute();
  const { handleHealthRoute } = useHealthRoute();

  const handleApiResult = useMemo(() => {
    let handler;
    switch (label) {
      case 'Users':
        handler = handleUserDataListRoute;
        break;
      case 'Partners':
        handler = handlePartnersRoute;
        break;
      case 'Health':
        handler = handleHealthRoute;
        break;
      default:
        handler = undefined;
    }
    return handler;
  }, [handleHealthRoute, handleUserDataListRoute, handlePartnersRoute, label]);

  return (
    <div style={{ cursor: 'pointer' }}>
      <MenuCard onClick={handleApiResult} label={label} Icon={Icon} />
    </div>
  );
}

HomeMenu.propTypes = {
  appUser: PropTypes.object,
};
HomeMenu.defaultProps = {
  appUser: null,
};
HomeMenuItem.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  Icon: PropTypes.node,
};
HomeMenuItem.defaultProps = {
  appUser: null,
  label: 'Exercise',
  Icon: 'running',
};

export default HomeMenu;
