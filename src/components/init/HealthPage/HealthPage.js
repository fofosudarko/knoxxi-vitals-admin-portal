import PropTypes from 'prop-types';

import HealthMenu from '../HealthMenu/HealthMenu';

function HealthPage({ appUser }) {
  return (
    <div>
      <div className="page-title">Health</div>
      <div className="page-content">
        <HealthMenu appUser={appUser} />
      </div>
    </div>
  );
}

HealthPage.propTypes = {
  appUser: PropTypes.object,
};
HealthPage.defaultProps = {
  appUser: null,
};

export default HealthPage;
