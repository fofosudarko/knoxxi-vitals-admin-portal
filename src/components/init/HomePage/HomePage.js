import PropTypes from 'prop-types';

import HomeMenu from '../HomeMenu/HomeMenu';

function HomePage({ appUser }) {
  return (
    <div>
      <div className="page-title">Home</div>
      <div className="page-content">
        <HomeMenu appUser={appUser} />
      </div>
    </div>
  );
}

HomePage.propTypes = {
  appUser: PropTypes.object,
};
HomePage.defaultProps = {
  appUser: null,
};

export default HomePage;
