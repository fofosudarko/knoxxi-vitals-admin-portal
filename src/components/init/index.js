import HomePage from './HomePage/HomePage';
import HomeMenu from './HomeMenu/HomeMenu';
import HealthPage from './HealthPage/HealthPage';
import HealthMenu from './HealthMenu/HealthMenu';

export { HomePage, HomeMenu, HealthPage, HealthMenu };
