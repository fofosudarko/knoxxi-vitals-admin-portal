import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  YUP_PARTNER_NAME_VALIDATOR,
  YUP_PARTNER_LOCATION_VALIDATOR,
  YUP_PARTNER_ALIAS_VALIDATOR,
} from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function PartnerInput({ partner, isEditing, onSubmit, processing }) {
  const partnerInputSchema = Yup({
    name: YUP_PARTNER_NAME_VALIDATOR,
    location: YUP_PARTNER_LOCATION_VALIDATOR,
    alias: YUP_PARTNER_ALIAS_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      name: partner?.name ?? '',
      location: partner?.location ?? '',
      alias: partner?.alias ?? '',
    }),
    [partner?.name, partner?.alias, partner?.location]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({ defaultValues, resolver: yupResolver(partnerInputSchema) });

  useFormReset({ values: defaultValues, item: partner, reset });

  const handleSubmitPartner = useCallback(
    ({ name, alias, location }) => {
      const newPartner = {
        name,
        alias,
        location,
      };

      onSubmit && onSubmit(newPartner);
    },
    [onSubmit]
  );

  const handleCancelPartner = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitPartner)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Partner name</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Partner name"
                  {...field}
                  isInvalid={!!errors.name}
                />
              )}
              name="name"
              control={control}
            />
            {errors.name ? (
              <Form.Control.Feedback type="invalid">
                {errors.name.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the partner a name</Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Partner location
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Partner location"
                  {...field}
                  isInvalid={!!errors.location}
                />
              )}
              name="location"
              control={control}
            />
            {errors.location ? (
              <Form.Control.Feedback type="invalid">
                {errors.location.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the partner a location</Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Partner alias</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Partner alias"
                  {...field}
                  isInvalid={!!errors.alias}
                />
              )}
              name="alias"
              control={control}
            />
            {errors.alias ? (
              <Form.Control.Feedback type="invalid">
                {errors.alias.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the partner an alias</Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelPartner}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit partner"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add partner"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

PartnerInput.propTypes = {
  appUser: PropTypes.object,
  partner: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
PartnerInput.defaultProps = {
  appUser: null,
  partner: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default PartnerInput;
