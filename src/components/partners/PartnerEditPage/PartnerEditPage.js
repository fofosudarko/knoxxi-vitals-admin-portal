import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useUpdatePartner } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';

import { ContentContainer, GoBack } from 'src/components/lib';
import PartnerInput from '../PartnerInput/PartnerInput';

function PartnerEditPage({ appUser, partner }) {
  const { handlePartnersRoute } = useRoutes().usePartnersRoute();
  const {
    handleUpdatePartner: _handleUpdatePartner,
    error,
    processing,
    setError,
    partnerUpdated,
    setPartnerUpdated,
  } = useUpdatePartner({ appUser });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (partnerUpdated) {
      handleApiResult();
    }
  }, [handleApiResult, partnerUpdated]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('Partner updated successfully').getNormalNotification(),
      () => {
        setPartnerUpdated(false);
      }
    );
    handlePartnersRoute();
  }, [handleNotification, handlePartnersRoute, setPartnerUpdated]);

  const handleUpdatePartner = useCallback(
    async (updatedPartner) => {
      await _handleUpdatePartner(partner, updatedPartner);
    },
    [_handleUpdatePartner, partner]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Edit partner</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <PartnerInput
          appUser={appUser}
          onSubmit={handleUpdatePartner}
          isEditing
          processing={processing}
          partner={partner}
        />
      </ContentContainer>
    </div>
  );
}

PartnerEditPage.propTypes = {
  appUser: PropTypes.object,
  partner: PropTypes.object,
};
PartnerEditPage.defaultProps = {
  appUser: null,
  partner: null,
};

export default PartnerEditPage;
