import PropTypes from 'prop-types';

import { PartnersListView } from '../PartnersList/PartnersList';

function PartnersPage({ appUser }) {
  const query = null;
  return (
    <div>
      <div className="page-title">Partners</div>
      <div className="page-content">
        <PartnersListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

PartnersPage.propTypes = {
  appUser: PropTypes.object,
};
PartnersPage.defaultProps = {
  appUser: null,
};

export default PartnersPage;
