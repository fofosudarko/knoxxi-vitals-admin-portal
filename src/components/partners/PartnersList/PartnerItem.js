import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';

import PartnerRemove from '../PartnerRemove/PartnerRemove';
import PartnerEditRoute from '../PartnerEdit/PartnerEditRoute';

export function usePartnerDetails(partner) {
  const { name, location, alias } = partner ?? {};

  return {
    name,
    location,
    alias,
  };
}

export function PartnerGridItem({ partner }) {
  const { name, location, alias } = usePartnerDetails(partner);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Name</div>
            <div className="item-subtitle">{name ? name : 'N/A'}</div>
          </Col>
          <Col>
            <div className="item-title">Location</div>
            <div className="item-subtitle">{location ? location : 'N/A'}</div>
          </Col>
          <Col>
            <div className="item-title">Alias</div>
            <div className="item-subtitle">{alias ? alias : 'N/A'}</div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              <PartnerItemActions partner={partner} />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function PartnerTableItem({ partner }) {
  const { name, location, alias } = usePartnerDetails(partner);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {name ? name : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {location ? location : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {alias ? alias : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          <PartnerItemActions partner={partner} />
        </div>
      </td>
    </tr>
  );
}

export function PartnerItemActions({ partner }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <PartnerEditRoute partner={partner} />
          </Dropdown.Item>
          <Dropdown.Item>
            <PartnerRemove partner={partner} />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

PartnerGridItem.propTypes = {
  partner: PropTypes.object,
};
PartnerGridItem.defaultProps = {
  partner: null,
};
PartnerTableItem.propTypes = {
  partner: PropTypes.object,
};
PartnerTableItem.defaultProps = {
  partner: null,
};
PartnerItemActions.propTypes = {
  partner: PropTypes.object,
};
PartnerItemActions.defaultProps = {
  partner: null,
};
