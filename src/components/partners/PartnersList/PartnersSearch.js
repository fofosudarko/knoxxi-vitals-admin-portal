import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_CONTENT_NAME_VALIDATOR } from 'src/config/validators';
import { useNotificationHandler } from 'src/hooks';
import { useSelectItems } from 'src/hooks/api';

import { FilterContainer } from 'src/components/lib';
import { PartnersList, PartnersListEmpty } from './_PartnersList';

function PartnersSearch({ query, onSearch, appUser }) {
  const [{ name }, setSearchTerms] = useState({});

  useEffect(() => {
    onSearch && onSearch(name);
  }, [onSearch, name]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div>
      <FilterContainer>
        <PartnersSearchInput onSubmit={handleSubmitSearchTerms} />
      </FilterContainer>
      {name ? (
        <div className="my-2">
          <PartnersSearchOutput name={name} query={query} appUser={appUser} />
        </div>
      ) : null}
    </div>
  );
}

function PartnersSearchInput({ onSubmit }) {
  const partnersSearchInputSchema = Yup({
    name: YUP_CONTENT_NAME_VALIDATOR,
  });

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(partnersSearchInputSchema),
  });

  const watchedName = watch('name');

  useEffect(() => {
    const name = watchedName;
    onSubmit && onSubmit({ name });
  }, [onSubmit, watchedName]);

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Controller
              name="name"
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Search partners by name"
                  {...field}
                  isInvalid={!!errors.name}
                />
              )}
            />
            {errors.name ? (
              <Form.Control.Feedback type="invalid">
                {errors.name.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}

function PartnersSearchOutput({ name, query, appUser }) {
  const { error, setError, partners, setPartners, handleListPartners } =
    useSelectItems();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    (async () => {
      await handleListPartners({
        ...query,
        name,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query, name]);

  useEffect(() => {
    return () => {
      setPartners(null);
    };
  }, [setPartners]);

  return (
    <PartnersList
      partners={partners}
      loadingText="Searching partners..."
      PartnersListEmpty={PartnersListEmpty}
      isSearch
      appUser={appUser}
    />
  );
}

PartnersSearch.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
PartnersSearch.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
PartnersSearchOutput.propTypes = {
  name: PropTypes.string,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
PartnersSearchOutput.defaultProps = {
  name: undefined,
  query: null,
  appUser: null,
};
PartnersSearchInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
};
PartnersSearchInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
};

export default PartnersSearch;
