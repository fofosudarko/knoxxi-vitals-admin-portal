import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetPartners } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function PartnersReload({ useTooltip }) {
  const _handleResetPartners = useResetPartners();

  const handleResetPartners = useCallback(() => {
    _handleResetPartners();
  }, [_handleResetPartners]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetPartners}
      text="Reload partners"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

PartnersReload.propTypes = {
  useTooltip: PropTypes.bool,
};
PartnersReload.defaultProps = {
  useTooltip: false,
};

export default PartnersReload;
