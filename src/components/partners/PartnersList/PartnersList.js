import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListPartners, useSearchItems } from 'src/hooks/api';

import PartnersReload from './PartnersReload';
import PartnerNewRoute from '../PartnerNew/PartnerNewRoute';
import { PartnersList, PartnersListEmpty } from './_PartnersList';
import PartnersSearch from './PartnersSearch';

function PartnersListContainer({ appUser, query }) {
  const {
    partners,
    error: partnersError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListPartners({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (partnersError) {
      handleNotification(partnersError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, partnersError, setError]);

  const handleLoadMorePartners = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <PartnersList
      partners={partners}
      onLoadMore={handleLoadMorePartners}
      loadingText="Loading partners..."
      appUser={appUser}
      PartnersListEmpty={PartnersListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function PartnersListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end my-1">
        <PartnerNewRoute />
      </div>
      <div className="d-flex justify-content-end">
        <PartnersReload useTooltip />
      </div>
      <div>
        <PartnersSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <PartnersListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

PartnersListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
PartnersListContainer.defaultProps = {
  appUser: null,
  query: null,
};
PartnersList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  partners: PropTypes.array,
  PartnersListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
PartnersList.defaultProps = {
  appUser: null,
  loadingText: 'Loading partners...',
  onLoadMore: undefined,
  partners: null,
  PartnersListEmpty: null,
  isDisabled: false,
};
PartnersListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
PartnersListView.defaultProps = {
  appUser: null,
  query: null,
};

export default PartnersList;
