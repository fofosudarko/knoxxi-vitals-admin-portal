import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { PartnerTableItem, PartnerGridItem } from './PartnerItem';

export function PartnersList({
  partners,
  loadingText,
  appUser,
  onLoadMore,
  PartnersListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!partners) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(PartnersListEmpty);

  return partners.length ? (
    <div>
      {isLargeDevice ? (
        <PartnersListTable partners={partners} appUser={appUser} />
      ) : (
        <PartnersListGrid partners={partners} appUser={appUser} />
      )}

      {partners.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more partners"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function PartnersListGrid({ partners, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {partners.map((item) => (
          <Col key={item.id}>
            <PartnerGridItem partner={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function PartnersListTable({ partners, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Name</th>
            <th className="list-table-head-cell">Location</th>
            <th className="list-table-head-cell">Alias</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {partners.map((item, index) => (
            <PartnerTableItem partner={item} appUser={appUser} key={index} />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function PartnersListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there are no partners yet.
          </div>
        </div>
      </div>
    </div>
  );
}

PartnersList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  partners: PropTypes.array,
  PartnersListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
PartnersList.defaultProps = {
  appUser: null,
  loadingText: 'Loading partners...',
  onLoadMore: undefined,
  partners: null,
  PartnersListEmpty: null,
  isDisabled: false,
};
PartnersListGrid.propTypes = {
  appUser: PropTypes.object,
  partners: PropTypes.array,
};
PartnersListGrid.defaultProps = {
  appUser: null,
  partners: null,
};
PartnersListTable.propTypes = {
  appUser: PropTypes.object,
  partners: PropTypes.array,
};
PartnersListTable.defaultProps = {
  appUser: null,
  partners: null,
};
PartnersListEmpty.propTypes = {
  appUser: PropTypes.object,
};
PartnersListEmpty.defaultProps = {
  appUser: null,
};
