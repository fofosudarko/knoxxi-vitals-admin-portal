import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreatePartner } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';

import { ContentContainer, GoBack } from 'src/components/lib';
import PartnerInput from '../PartnerInput/PartnerInput';

function PartnerNewPage({ appUser }) {
  const { handlePartnersRoute } = useRoutes().usePartnersRoute();
  const {
    handleCreatePartner: _handleCreatePartner,
    error,
    setError,
    processing,
    partnerCreated,
    setPartnerCreated,
  } = useCreatePartner({ appUser });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (partnerCreated) {
      handleApiResult();
    }
  }, [partnerCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('Partner created successfully').getSuccessNotification(),
      () => {
        setPartnerCreated(false);
      }
    );
    handlePartnersRoute();
  }, [handleNotification, handlePartnersRoute, setPartnerCreated]);

  const handleCreatePartner = useCallback(
    async (partner) => {
      const body = partner;
      await _handleCreatePartner(body);
    },
    [_handleCreatePartner]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New partner</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <PartnerInput
          appUser={appUser}
          onSubmit={handleCreatePartner}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

PartnerNewPage.propTypes = {
  appUser: PropTypes.object,
};
PartnerNewPage.defaultProps = {
  appUser: null,
};

export default PartnerNewPage;
