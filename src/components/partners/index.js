import PartnersList, { PartnersListView } from './PartnersList/PartnersList';
import { PartnerItemActions } from './PartnersList/PartnerItem';
import PartnerInput from './PartnerInput/PartnerInput';
import PartnerRemove from './PartnerRemove/PartnerRemove';
import PartnersPage from './PartnersPage/PartnersPage';
import PartnerEditRoute from './PartnerEdit/PartnerEditRoute';
import PartnerEditPage from './PartnerEditPage/PartnerEditPage';
import PartnerNewRoute from './PartnerNew/PartnerNewRoute';
import PartnerNewPage from './PartnerNewPage/PartnerNewPage';

export {
  PartnersList,
  PartnerInput,
  PartnerRemove,
  PartnersPage,
  PartnersListView,
  PartnerEditRoute,
  PartnerEditPage,
  PartnerItemActions,
  PartnerNewRoute,
  PartnerNewPage,
};
