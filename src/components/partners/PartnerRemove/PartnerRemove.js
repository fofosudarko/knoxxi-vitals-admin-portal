import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemovePartner } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function PartnerRemove({ partner, appUser, useTooltip }) {
  const {
    handleRemovePartner: _handleRemovePartner,
    error,
    setError,
    partnerRemoved,
    setPartnerRemoved,
  } = useRemovePartner({ appUser });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (partnerRemoved) {
      handleApiResult();
    }
  }, [partnerRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('Partner removed successfully').getAlertNotification()
    );
    setPartnerRemoved(false);
  }, [handleNotification, setPartnerRemoved]);

  const handleRemovePartner = useCallback(async () => {
    await _handleRemovePartner(partner);
  }, [_handleRemovePartner, partner]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemovePartner}
        variant="white"
        text="Remove partner"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

PartnerRemove.propTypes = {
  partner: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
PartnerRemove.defaultProps = {
  partner: null,
  appUser: null,
  useTooltip: false,
};

export default PartnerRemove;
