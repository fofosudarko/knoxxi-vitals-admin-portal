import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { EditButton } from 'src/components/lib';

export default function PartnerEditRoute({ useTooltip, partner }) {
  const { handlePartnerEditRoute } = useRoutes().usePartnerEditRoute(partner);

  return (
    <EditButton
      onClick={handlePartnerEditRoute}
      variant="transparent"
      text="Edit partner"
      textColor="black"
      textNormal
      autoWidth={!useTooltip}
      useTooltip={useTooltip}
    />
  );
}

PartnerEditRoute.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
  partner: PropTypes.object,
};
PartnerEditRoute.defaultProps = {
  useTooltip: false,
  appUser: null,
  partner: null,
};
