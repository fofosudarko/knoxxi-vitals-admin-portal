import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function PartnerNewRoute({ text }) {
  const { handlePartnerNewRoute } = useRoutes().usePartnerNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handlePartnerNewRoute}
      text={text}
    />
  );
}

PartnerNewRoute.propTypes = {
  text: PropTypes.string,
};
PartnerNewRoute.defaultProps = {
  text: 'New partner',
};
