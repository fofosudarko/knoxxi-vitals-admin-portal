import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetGlucoseDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function GlucoseDataListReload({ useTooltip }) {
  const _handleResetGlucoseDataList = useResetGlucoseDataList();

  const handleResetGlucoseDataList = useCallback(() => {
    _handleResetGlucoseDataList();
  }, [_handleResetGlucoseDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetGlucoseDataList}
      text="Reload glucose data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

GlucoseDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
GlucoseDataListReload.defaultProps = {
  useTooltip: false,
};

export default GlucoseDataListReload;
