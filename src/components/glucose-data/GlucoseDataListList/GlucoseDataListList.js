import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListGlucoseDataList, useSearchItems } from 'src/hooks/api';

import GlucoseDataListReload from './GlucoseDataListReload';
import {
  GlucoseDataListList,
  GlucoseDataListListEmpty,
} from './_GlucoseDataListList';
import GlucoseDataListSearch from './GlucoseDataListSearch';

function GlucoseDataListListContainer({ appUser, query }) {
  const {
    glucoseDataList,
    error: glucoseDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListGlucoseDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (glucoseDataListError) {
      handleNotification(glucoseDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, glucoseDataListError, setError]);

  const handleLoadMoreGlucoseDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <GlucoseDataListList
      glucoseDataList={glucoseDataList}
      onLoadMore={handleLoadMoreGlucoseDataList}
      loadingText="Loading glucose data list..."
      appUser={appUser}
      GlucoseDataListListEmpty={GlucoseDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function GlucoseDataListListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <GlucoseDataListReload useTooltip />
      </div>
      <div>
        <GlucoseDataListSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <GlucoseDataListListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

GlucoseDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
GlucoseDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
GlucoseDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  glucoseDataList: PropTypes.array,
  GlucoseDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
GlucoseDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading glucose data list...',
  onLoadMore: undefined,
  glucoseDataList: null,
  GlucoseDataListListEmpty: null,
  isDisabled: false,
};
GlucoseDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
GlucoseDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default GlucoseDataListList;
