import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_CONTENT_NAME_VALIDATOR } from 'src/config/validators';
import { useNotificationHandler } from 'src/hooks';
import { useSelectItems } from 'src/hooks/api';

import { FilterContainer } from 'src/components/lib';
import {
  GlucoseDataListList,
  GlucoseDataListListEmpty,
} from './_GlucoseDataListList';

function GlucoseDataListSearch({ query, onSearch, appUser }) {
  const [{ userDataFullname }, setSearchTerms] = useState({});

  useEffect(() => {
    onSearch && onSearch(userDataFullname);
  }, [onSearch, userDataFullname]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div>
      <FilterContainer>
        <GlucoseDataListSearchInput onSubmit={handleSubmitSearchTerms} />
      </FilterContainer>
      {userDataFullname ? (
        <div className="my-2">
          <GlucoseDataListSearchOutput
            userDataFullname={userDataFullname}
            query={query}
            appUser={appUser}
          />
        </div>
      ) : null}
    </div>
  );
}

function GlucoseDataListSearchInput({ onSubmit }) {
  const glucoseDataListSearchInputSchema = Yup({
    userDataFullname: YUP_CONTENT_NAME_VALIDATOR,
  });

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(glucoseDataListSearchInputSchema),
  });

  const watchedUserDataFullname = watch('userDataFullname');

  useEffect(() => {
    const userDataFullname = watchedUserDataFullname;
    onSubmit && onSubmit({ userDataFullname });
  }, [onSubmit, watchedUserDataFullname]);

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Controller
              name="userDataFullname"
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Search glucose data list by user fullname"
                  {...field}
                  isInvalid={!!errors.userDataFullname}
                />
              )}
            />
            {errors.userDataFullname ? (
              <Form.Control.Feedback type="invalid">
                {errors.userDataFullname.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}

function GlucoseDataListSearchOutput({ userDataFullname, query, appUser }) {
  const {
    error,
    setError,
    glucoseDataList,
    setGlucoseDataList,
    handleListGlucoseDataList,
  } = useSelectItems();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    (async () => {
      await handleListGlucoseDataList({
        ...query,
        userDataFullname,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query, userDataFullname]);

  useEffect(() => {
    return () => {
      setGlucoseDataList(null);
    };
  }, [setGlucoseDataList]);

  return (
    <GlucoseDataListList
      glucoseDataList={glucoseDataList}
      loadingText="Searching glucose data list..."
      GlucoseDataListListEmpty={GlucoseDataListListEmpty}
      isSearch
      appUser={appUser}
    />
  );
}

GlucoseDataListSearch.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
GlucoseDataListSearch.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
GlucoseDataListSearchOutput.propTypes = {
  userDataFullname: PropTypes.string,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
GlucoseDataListSearchOutput.defaultProps = {
  userDataFullname: undefined,
  query: null,
  appUser: null,
};
GlucoseDataListSearchInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
};
GlucoseDataListSearchInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
};

export default GlucoseDataListSearch;
