import { useRef } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Chart } from 'react-chartjs-2';
import 'chart.js/auto';

ChartJS.register(ArcElement, Tooltip, Legend);

function ChartCard({
  onClick,
  label,
  labelColor,
  textNormal,
  shadowless,
  chartProps,
}) {
  const chartRef = useRef();
  return (
    <Card
      onClick={onClick}
      className={`w-100 ${
        !shadowless ? 'shadow-sm' : undefined
      } border border-light bg-count-card`}
      style={{ cursor: onClick ? 'pointer' : 'not-allowed' }}
    >
      <Card.Body>
        <div>
          <Chart ref={chartRef} {...chartProps} />
        </div>
        <Card.Title
          className={`mt-2 text-${labelColor} ${
            textNormal ? 'fw-normal' : 'fw-bold'
          }  text-center`}
          as="div"
        >
          {label}
        </Card.Title>
      </Card.Body>
    </Card>
  );
}

ChartCard.propTypes = {
  onClick: PropTypes.func,
  label: PropTypes.string,
  labelColor: PropTypes.string,
  textNormal: PropTypes.bool,
  shadowless: PropTypes.bool,
  chartProps: PropTypes.object,
};
ChartCard.defaultProps = {
  onClick: undefined,
  label: 'Chart',
  labelColor: 'secondary',
  textNormal: false,
  shadowless: false,
  chartProps: {
    type: 'bar',
    options: null,
    data: null,
  },
};

export default ChartCard;
