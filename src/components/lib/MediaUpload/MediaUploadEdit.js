import { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useUpdateMedia } from 'src/hooks/api';

import { EditButton } from 'src/components/lib';
import MediaUploadInput from './MediaUploadInput';

function MediaUploadEdit({
  appUser,
  media,
  onReceiveMedia,
  editText,
  editLabel,
}) {
  const {
    handleUpdateMedia,
    processing,
    error,
    setError,
    media: newMedia,
    setMedia,
    mediaUpdated,
    setMediaUpdated,
  } = useUpdateMedia();
  const handleNotification = useNotificationHandler();
  let handleHide;

  const [showMediaUploadEdit, setShowMediaUploadEdit] = useState(false);

  useEffect(() => {
    if (error) {
      handleNotification(error);
      handleHide();
    }
    return () => {
      setError(null);
    };
  }, [error, handleNotification, handleHide, setError]);

  useEffect(() => {
    if (mediaUpdated) {
      handleHide();
    }
  }, [mediaUpdated, handleHide]);

  const handleShowMediaUploadEdit = useCallback(() => {
    setShowMediaUploadEdit((state) => !state);
  }, []);

  handleHide = useCallback(() => {
    onReceiveMedia && onReceiveMedia(newMedia);
    setShowMediaUploadEdit(false);
    setMediaUpdated(false);
    setMedia(null);
  }, [newMedia, onReceiveMedia, setMedia, setMediaUpdated]);

  const handleMediaUploadEdit = useCallback(
    async (updatedFiles) => {
      const formData = new FormData();
      formData.append('media', updatedFiles[0]);
      await handleUpdateMedia(media, formData);
    },
    [handleUpdateMedia, media]
  );

  return (
    <div>
      {showMediaUploadEdit ? (
        <MediaUploadInput
          media={media}
          appUser={appUser}
          onSubmit={handleMediaUploadEdit}
          isEditing
          processing={processing}
          label={editLabel}
        />
      ) : (
        <EditButton
          variant="transparent"
          onClick={handleShowMediaUploadEdit}
          textColor="black"
          text={editText}
          textNormal
          autoWidth
        />
      )}
    </div>
  );
}

MediaUploadEdit.propTypes = {
  appUser: PropTypes.object,
  media: PropTypes.object,
  onReceiveMedia: PropTypes.func,
  editText: PropTypes.string,
  editLabel: PropTypes.string,
};
MediaUploadEdit.defaultProps = {
  appUser: null,
  media: null,
  onReceiveMedia: undefined,
  editText: 'Edit media',
  editLabel: 'Upload media',
};

export default MediaUploadEdit;
