import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetBPDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function BPDataListReload({ useTooltip }) {
  const _handleResetBPDataList = useResetBPDataList();

  const handleResetBPDataList = useCallback(() => {
    _handleResetBPDataList();
  }, [_handleResetBPDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetBPDataList}
      text="Reload BP data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

BPDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
};
BPDataListReload.defaultProps = {
  useTooltip: false,
};

export default BPDataListReload;
