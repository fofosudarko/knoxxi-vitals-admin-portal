import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListBPDataList, useSearchItems } from 'src/hooks/api';

import BPDataListReload from './BPDataListReload';
import { BPDataListList, BPDataListListEmpty } from './_BPDataListList';
import BPDataListSearch from './BPDataListSearch';

function BPDataListListContainer({ appUser, query }) {
  const {
    bpDataList,
    error: bpDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListBPDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (bpDataListError) {
      handleNotification(bpDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, bpDataListError, setError]);

  const handleLoadMoreBPDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <BPDataListList
      bpDataList={bpDataList}
      onLoadMore={handleLoadMoreBPDataList}
      loadingText="Loading BP data list..."
      appUser={appUser}
      BPDataListListEmpty={BPDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function BPDataListListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <BPDataListReload useTooltip />
      </div>
      <div>
        <BPDataListSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <BPDataListListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

BPDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
BPDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
BPDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  bpDataList: PropTypes.array,
  BPDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
BPDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading BP data list...',
  onLoadMore: undefined,
  bpDataList: null,
  BPDataListListEmpty: null,
  isDisabled: false,
};
BPDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
BPDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default BPDataListList;
