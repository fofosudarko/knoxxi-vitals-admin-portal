import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_CONTENT_NAME_VALIDATOR } from 'src/config/validators';
import { useNotificationHandler } from 'src/hooks';
import { useSelectItems } from 'src/hooks/api';

import { FilterContainer } from 'src/components/lib';
import { BPDataListList, BPDataListListEmpty } from './_BPDataListList';

function BPDataListSearch({ query, onSearch, appUser }) {
  const [{ userDataFullname }, setSearchTerms] = useState({});

  useEffect(() => {
    onSearch && onSearch(userDataFullname);
  }, [onSearch, userDataFullname]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div>
      <FilterContainer>
        <BPDataListSearchInput onSubmit={handleSubmitSearchTerms} />
      </FilterContainer>
      {userDataFullname ? (
        <div className="my-2">
          <BPDataListSearchOutput
            userDataFullname={userDataFullname}
            query={query}
            appUser={appUser}
          />
        </div>
      ) : null}
    </div>
  );
}

function BPDataListSearchInput({ onSubmit }) {
  const bpDataListSearchInputSchema = Yup({
    userDataFullname: YUP_CONTENT_NAME_VALIDATOR,
  });

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(bpDataListSearchInputSchema),
  });

  const watchedUserDataFullname = watch('userDataFullname');

  useEffect(() => {
    const userDataFullname = watchedUserDataFullname;
    onSubmit && onSubmit({ userDataFullname });
  }, [onSubmit, watchedUserDataFullname]);

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Controller
              name="userDataFullname"
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Search BP data list by user fullname"
                  {...field}
                  isInvalid={!!errors.userDataFullname}
                />
              )}
            />
            {errors.userDataFullname ? (
              <Form.Control.Feedback type="invalid">
                {errors.userDataFullname.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}

function BPDataListSearchOutput({ userDataFullname, query, appUser }) {
  const { error, setError, bpDataList, setBPDataList, handleListBPDataList } =
    useSelectItems();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    (async () => {
      await handleListBPDataList({
        ...query,
        userDataFullname,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query, userDataFullname]);

  useEffect(() => {
    return () => {
      setBPDataList(null);
    };
  }, [setBPDataList]);

  return (
    <BPDataListList
      bpDataList={bpDataList}
      loadingText="Searching BP data list..."
      BPDataListListEmpty={BPDataListListEmpty}
      isSearch
      appUser={appUser}
    />
  );
}

BPDataListSearch.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
BPDataListSearch.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
BPDataListSearchOutput.propTypes = {
  userDataFullname: PropTypes.string,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
BPDataListSearchOutput.defaultProps = {
  userDataFullname: undefined,
  query: null,
  appUser: null,
};
BPDataListSearchInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
};
BPDataListSearchInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
};

export default BPDataListSearch;
