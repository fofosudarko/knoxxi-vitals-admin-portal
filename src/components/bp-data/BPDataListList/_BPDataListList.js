import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { BPDataTableItem, BPDataGridItem } from './BPDataItem';

export function BPDataListList({
  bpDataList,
  loadingText,
  appUser,
  onLoadMore,
  BPDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!bpDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(BPDataListListEmpty);

  return bpDataList.length ? (
    <div>
      {isLargeDevice ? (
        <BPDataListListTable bpDataList={bpDataList} appUser={appUser} />
      ) : (
        <BPDataListListGrid bpDataList={bpDataList} appUser={appUser} />
      )}

      {bpDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more BP data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function BPDataListListGrid({ bpDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {bpDataList.map((item) => (
          <Col key={item.id}>
            <BPDataGridItem bpData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function BPDataListListTable({ bpDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Systolic(mmHg)</th>
            <th className="list-table-head-cell">Diastolic(mmHg)</th>
            <th className="list-table-head-cell">Pulse(bpm)</th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {bpDataList.map((item, index) => (
            <BPDataTableItem bpData={item} appUser={appUser} key={index} />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function BPDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no BP data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

BPDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  bpDataList: PropTypes.array,
  BPDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
BPDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading BP data list...',
  onLoadMore: undefined,
  bpDataList: null,
  BPDataListListEmpty: null,
  isDisabled: false,
};
BPDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  bpDataList: PropTypes.array,
};
BPDataListListGrid.defaultProps = {
  appUser: null,
  bpDataList: null,
};
BPDataListListTable.propTypes = {
  appUser: PropTypes.object,
  bpDataList: PropTypes.array,
};
BPDataListListTable.defaultProps = {
  appUser: null,
  bpDataList: null,
};
BPDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
BPDataListListEmpty.defaultProps = {
  appUser: null,
};
