import PropTypes from 'prop-types';

import { BPDataListListView } from '../BPDataListList/BPDataListList';

function BPDataListPage({ appUser }) {
  const query = null;
  return (
    <div>
      <div className="page-title">BP data</div>
      <div className="page-content">
        <BPDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

BPDataListPage.propTypes = {
  appUser: PropTypes.object,
};
BPDataListPage.defaultProps = {
  appUser: null,
};

export default BPDataListPage;
