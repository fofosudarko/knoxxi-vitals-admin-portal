import { useEffect, useState, useCallback, useContext } from 'react';
import httpStatus from 'http-status';

import { isAxiosError } from 'src/utils';
import useRoutes from './routes';
import useNotificationHandler from './notification';
import { useRedirectRoute } from './page';
import { useAuthStore } from 'src/stores/auth';
import { useDashboardStore } from 'src/stores/dashboard';
import { useBPDataStore } from 'src/stores/bp-data';
import { useUserDataStore } from 'src/stores/user-data';
import { useGlucoseDataStore } from 'src/stores/glucose-data';
import { useTemperatureDataStore } from 'src/stores/temperature-data';
import { usePartnerStore } from 'src/stores/partner';
import { useWeightDataStore } from 'src/stores/weight-data';
import { useWaterDataStore } from 'src/stores/water-data';
import { useOxygenDataStore } from 'src/stores/oxygen-data';
import { useCholesterolDataStore } from 'src/stores/cholesterol-data';
import useHasMounted from './has-mounted';

import { AppContext } from 'src/context/app';

export function useAuth() {
  const handleSignOut = useSignOut();
  const { appUser } = useAuthStore((state) => state);
  const hasMounted = useHasMounted();

  const [error, setError] = useState(null);

  useEffect(() => {
    if (!appUser) {
      (async () => {
        try {
          await handleSignOut();
        } catch (error) {
          setError(error);
        }
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appUser]);

  return { error, appUser, hasMounted };
}

export function useSignOut() {
  const { clearAppState } = useContext(AppContext);
  const { handleSignInRoute } = useRoutes().useSignInRoute();
  const handleNotification = useNotificationHandler();
  const clearAuthState = useAuthStore((state) => state.clearAuthState);
  const clearDashboardState = useDashboardStore(
    (state) => state.clearDashboardState
  );
  const clearBPDataState = useBPDataStore((state) => state.clearBPDataState);
  const clearUserDataState = useUserDataStore(
    (state) => state.clearUserDataState
  );
  const clearGlucoseDataState = useGlucoseDataStore(
    (state) => state.clearGlucoseDataState
  );
  const clearTemperatureDataState = useTemperatureDataStore(
    (state) => state.clearTemperatureDataState
  );
  const clearPartnerState = usePartnerStore((state) => state.clearPartnerState);
  const clearWeightDataState = useWeightDataStore(
    (state) => state.clearWeightDataState
  );
  const clearOxygenDataState = useOxygenDataStore(
    (state) => state.clearOxygenDataState
  );
  const clearWaterDataState = useWaterDataStore(
    (state) => state.clearWaterDataState
  );
  const clearCholesterolDataState = useCholesterolDataStore(
    (state) => state.clearCholesterolDataState
  );

  const handleSignOut = useCallback(async () => {
    try {
      handleSignInRoute();
    } catch (error) {
      handleNotification(error);
    }

    clearAuthState();
    clearDashboardState();
    clearBPDataState();
    clearUserDataState();
    clearGlucoseDataState();
    clearTemperatureDataState();
    clearPartnerState();
    clearWeightDataState();
    clearOxygenDataState();
    clearWaterDataState();
    clearCholesterolDataState();

    clearAppState();
  }, [
    clearAuthState,
    clearDashboardState,
    clearBPDataState,
    clearUserDataState,
    clearGlucoseDataState,
    clearTemperatureDataState,
    clearPartnerState,
    clearWeightDataState,
    clearOxygenDataState,
    clearWaterDataState,
    clearCholesterolDataState,
    clearAppState,
    handleSignInRoute,
    handleNotification,
  ]);

  return handleSignOut;
}

export function useHomeRedirect() {
  const appUser = useAuthStore((state) => state.appUser);
  const {
    handleRedirectRoute,
    error: redirectRouteError,
    setError: setRedirectRouteError,
  } = useRedirectRoute();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (redirectRouteError) {
      handleNotification(redirectRouteError);
    }

    return () => {
      setRedirectRouteError(null);
    };
  }, [handleNotification, redirectRouteError, setRedirectRouteError]);

  useEffect(() => {
    if (appUser?.account) {
      handleRedirectRoute();
    }
  }, [appUser, handleRedirectRoute]);
}

export function useForceSignOut() {
  const handleSignOut = useSignOut();

  const handleForceSignOut = useCallback(
    (error = null) => {
      if (
        error &&
        isAxiosError(error) &&
        error.response.status === httpStatus.FORBIDDEN
      ) {
        handleSignOut();
      }
    },
    [handleSignOut]
  );

  return handleForceSignOut;
}
