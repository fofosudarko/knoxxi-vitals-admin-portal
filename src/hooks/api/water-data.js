import { useCallback, useEffect } from 'react';

import { WaterDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useWaterDataStore } from 'src/stores/water-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListWaterDataList() {
  const listKey = generateListKey();
  const {
    waterDataList: _waterDataList,
    waterDataPage: __page,
    setWaterDataPage: _setPage,
    waterDataEndPaging: __endPaging,
    setWaterDataEndPaging: _setEndPaging,
    resetWaterDataPage: resetPage,
    listWaterDataList,
  } = useWaterDataStore((state) => state);
  const { error, setError } = useItem();
  const waterDataList = _waterDataList
    ? _waterDataList[listKey]
    : _waterDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!waterDataList?.length) {
      (async () => {
        await handleListWaterDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [waterDataList?.length]);

  useEffect(() => {
    if (waterDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListWaterDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListWaterDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await WaterDataService.listWaterDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const waterDataList = data.data;
          if (waterDataList?.length || page === 1) {
            listWaterDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listWaterDataList,
      setError,
    ]
  );

  return {
    handleListWaterDataList,
    waterDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetWaterData({
  waterData: _waterData = null,
  ignoreLoadOnMount = false,
}) {
  const { waterData, setWaterData } = useWaterDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetWaterData(_waterData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetWaterData = useCallback(
    async (waterData = null) => {
      try {
        const response = await WaterDataService.getWaterData(waterData);
        const data = await response.json();
        if (response.ok) {
          const waterData = data.data;
          setWaterData(waterData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setWaterData, setError]
  );

  return {
    handleGetWaterData,
    error,
    setError,
    processing,
    setProcessing,
    waterData,
  };
}

export function useFindWaterData({
  waterData: _waterData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    waterDataList: _waterDataList,
    setWaterData,
    waterData,
  } = useWaterDataStore((state) => state);
  const waterDataList = _waterDataList
    ? _waterDataList[listKey]
    : _waterDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetWaterData } = useGetWaterData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindWaterData(_waterData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindWaterData = useCallback(
    async (waterData = null) => {
      try {
        if (waterDataList?.length) {
          setWaterData(waterDataList.find((item) => item.id === waterData?.id));
        } else {
          await handleGetWaterData(waterData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [waterDataList, setWaterData, handleGetWaterData, setError]
  );

  return {
    handleFindWaterData,
    error,
    setError,
    processing,
    setProcessing,
    waterData,
  };
}

export function useRemoveWaterData() {
  const listKey = generateListKey();
  const {
    item: waterData,
    setItem: setWaterData,
    itemRemoved: waterDataRemoved,
    setItemRemoved: setWaterDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeWaterData } = useWaterDataStore((state) => state);

  const handleRemoveWaterData = useCallback(
    async (waterData = null) => {
      try {
        setProcessing(true);
        const response = await WaterDataService.removeWaterData(waterData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setWaterData(waterData);
            setWaterDataRemoved(true);
            removeWaterData({
              key: listKey,
              item: waterData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setWaterData,
      setWaterDataRemoved,
      removeWaterData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveWaterData,
    error,
    setError,
    processing,
    setProcessing,
    waterData,
    setWaterData,
    waterDataRemoved,
    setWaterDataRemoved,
  };
}

export function useResetWaterDataList() {
  const listKey = generateListKey();
  const { resetWaterDataPage: resetPage, clearWaterDataList } =
    useWaterDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetWaterDataList = useCallback(() => {
    handleResetPage();
    clearWaterDataList({ key: listKey });
  }, [handleResetPage, clearWaterDataList, listKey]);

  return handleResetWaterDataList;
}
