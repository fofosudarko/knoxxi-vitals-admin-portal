import { useCallback, useEffect } from 'react';

import { UserDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useUserDataStore } from 'src/stores/user-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListUserDataList() {
  const listKey = generateListKey();
  const {
    userDataList: _userDataList,
    userDataPage: __page,
    setUserDataPage: _setPage,
    userDataEndPaging: __endPaging,
    setUserDataEndPaging: _setEndPaging,
    resetUserDataPage: resetPage,
    listUserDataList,
  } = useUserDataStore((state) => state);
  const { error, setError } = useItem();
  const userDataList = _userDataList ? _userDataList[listKey] : _userDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!userDataList?.length) {
      (async () => {
        await handleListUserDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDataList?.length]);

  useEffect(() => {
    if (userDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListUserDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListUserDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await UserDataService.listUserDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const userDataList = data.data;
          if (userDataList?.length || page === 1) {
            listUserDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listUserDataList,
      setError,
    ]
  );

  return {
    handleListUserDataList,
    userDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetUserData({
  userData: _userData = null,
  ignoreLoadOnMount = false,
}) {
  const { userData, setUserData } = useUserDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetUserData(_userData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetUserData = useCallback(
    async (userData = null) => {
      try {
        const response = await UserDataService.getUserData(userData);
        const data = await response.json();
        if (response.ok) {
          const userData = data.data;
          setUserData(userData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setUserData, setError]
  );

  return {
    handleGetUserData,
    error,
    setError,
    processing,
    setProcessing,
    userData,
  };
}

export function useFindUserData({
  userData: _userData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    userDataList: _userDataList,
    setUserData,
    userData,
  } = useUserDataStore((state) => state);
  const userDataList = _userDataList ? _userDataList[listKey] : _userDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetUserData } = useGetUserData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindUserData(_userData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindUserData = useCallback(
    async (userData = null) => {
      try {
        if (userDataList?.length) {
          setUserData(userDataList.find((item) => item.id === userData?.id));
        } else {
          await handleGetUserData(userData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [userDataList, setUserData, handleGetUserData, setError]
  );

  return {
    handleFindUserData,
    error,
    setError,
    processing,
    setProcessing,
    userData,
  };
}

export function useRemoveUserData() {
  const listKey = generateListKey();
  const {
    item: userData,
    setItem: setUserData,
    itemRemoved: userDataRemoved,
    setItemRemoved: setUserDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeUserData } = useUserDataStore((state) => state);

  const handleRemoveUserData = useCallback(
    async (userData = null) => {
      try {
        setProcessing(true);
        const response = await UserDataService.removeUserData(userData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setUserData(userData);
            setUserDataRemoved(true);
            removeUserData({ key: listKey, item: userData });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setUserData,
      setUserDataRemoved,
      removeUserData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveUserData,
    error,
    setError,
    processing,
    setProcessing,
    userData,
    setUserData,
    userDataRemoved,
    setUserDataRemoved,
  };
}

export function useResetUserDataList() {
  const listKey = generateListKey();
  const { resetUserDataPage: resetPage, clearUserDataList } = useUserDataStore(
    (state) => state
  );

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetUserDataList = useCallback(() => {
    handleResetPage();
    clearUserDataList({ key: listKey });
  }, [handleResetPage, clearUserDataList, listKey]);

  return handleResetUserDataList;
}
