import { useCallback, useEffect } from 'react';

import { BPDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useBPDataStore } from 'src/stores/bp-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListBPDataList() {
  const listKey = generateListKey();
  const {
    bpDataList: _bpDataList,
    bpDataPage: __page,
    setBPDataPage: _setPage,
    bpDataEndPaging: __endPaging,
    setBPDataEndPaging: _setEndPaging,
    resetBPDataPage: resetPage,
    listBPDataList,
  } = useBPDataStore((state) => state);
  const { error, setError } = useItem();
  const bpDataList = _bpDataList ? _bpDataList[listKey] : _bpDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!bpDataList?.length) {
      (async () => {
        await handleListBPDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bpDataList?.length]);

  useEffect(() => {
    if (bpDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListBPDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListBPDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await BPDataService.listBPDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const bpDataList = data.data;
          if (bpDataList?.length || page === 1) {
            listBPDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listBPDataList,
      setError,
    ]
  );

  return {
    handleListBPDataList,
    bpDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetBPData({
  bpData: _bpData = null,
  ignoreLoadOnMount = false,
}) {
  const { bpData, setBPData } = useBPDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetBPData(_bpData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetBPData = useCallback(
    async (bpData = null) => {
      try {
        const response = await BPDataService.getBPData(bpData);
        const data = await response.json();
        if (response.ok) {
          const bpData = data.data;
          setBPData(bpData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setBPData, setError]
  );

  return {
    handleGetBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
  };
}

export function useFindBPData({
  bpData: _bpData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    bpDataList: _bpDataList,
    setBPData,
    bpData,
  } = useBPDataStore((state) => state);
  const bpDataList = _bpDataList ? _bpDataList[listKey] : _bpDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetBPData } = useGetBPData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindBPData(_bpData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindBPData = useCallback(
    async (bpData = null) => {
      try {
        if (bpDataList?.length) {
          setBPData(bpDataList.find((item) => item.id === bpData?.id));
        } else {
          await handleGetBPData(bpData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [bpDataList, setBPData, handleGetBPData, setError]
  );

  return {
    handleFindBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
  };
}

export function useRemoveBPData() {
  const listKey = generateListKey();
  const {
    item: bpData,
    setItem: setBPData,
    itemRemoved: bpDataRemoved,
    setItemRemoved: setBPDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeBPData } = useBPDataStore((state) => state);

  const handleRemoveBPData = useCallback(
    async (bpData = null) => {
      try {
        setProcessing(true);
        const response = await BPDataService.removeBPData(bpData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setBPData(bpData);
            setBPDataRemoved(true);
            removeBPData({ key: listKey, item: bpData });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setBPData,
      setBPDataRemoved,
      removeBPData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
    setBPData,
    bpDataRemoved,
    setBPDataRemoved,
  };
}

export function useResetBPDataList() {
  const listKey = generateListKey();
  const { resetBPDataPage: resetPage, clearBPDataList } = useBPDataStore(
    (state) => state
  );

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetBPDataList = useCallback(() => {
    handleResetPage();
    clearBPDataList({ key: listKey });
  }, [handleResetPage, clearBPDataList, listKey]);

  return handleResetBPDataList;
}
