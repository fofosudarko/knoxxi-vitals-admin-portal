import { useCallback, useEffect } from 'react';

import { PartnerService } from 'src/api/app/health';

import { usePager } from '../page';
import { usePartnerStore } from 'src/stores/partner';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListPartners() {
  const listKey = generateListKey();
  const {
    partners: _partners,
    partnerPage: __page,
    setPartnerPage: _setPage,
    partnerEndPaging: __endPaging,
    setPartnerEndPaging: _setEndPaging,
    resetPartnerPage: resetPage,
    listPartners,
  } = usePartnerStore((state) => state);
  const { error, setError } = useItem();
  const partners = _partners ? _partners[listKey] : _partners;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!partners?.length) {
      (async () => {
        await handleListPartners({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [partners?.length]);

  useEffect(() => {
    if (partners?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListPartners({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListPartners = useCallback(
    async ({ page = null }) => {
      try {
        const response = await PartnerService.listPartners({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const partners = data.data;
          if (partners?.length || page === 1) {
            listPartners({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listPartners,
      setError,
    ]
  );

  return {
    handleListPartners,
    partners,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetPartner({
  partner: _partner = null,
  ignoreLoadOnMount = false,
}) {
  const { partner, setPartner } = usePartnerStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetPartner(_partner);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetPartner = useCallback(
    async (partner = null) => {
      try {
        const response = await PartnerService.getPartner(partner);
        const data = await response.json();
        if (response.ok) {
          const partner = data.data;
          setPartner(partner);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setPartner, setError]
  );

  return {
    handleGetPartner,
    error,
    setError,
    processing,
    setProcessing,
    partner,
  };
}

export function useFindPartner({
  partner: _partner = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    partners: _partners,
    setPartner,
    partner,
  } = usePartnerStore((state) => state);
  const partners = _partners ? _partners[listKey] : _partners;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetPartner } = useGetPartner({ ignoreLoadOnMount: true });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindPartner(_partner);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindPartner = useCallback(
    async (partner = null) => {
      try {
        if (partners?.length) {
          setPartner(partners.find((item) => item.id === partner?.id));
        } else {
          await handleGetPartner(partner);
        }
      } catch (error) {
        setError(error);
      }
    },
    [partners, setPartner, handleGetPartner, setError]
  );

  return {
    handleFindPartner,
    error,
    setError,
    processing,
    setProcessing,
    partner,
  };
}

export function useCreatePartner() {
  const listKey = generateListKey();
  const {
    item: partner,
    setItem: setPartner,
    itemCreated: partnerCreated,
    setItemCreated: setPartnerCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addPartner } = usePartnerStore((state) => state);

  const handleCreatePartner = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await PartnerService.createPartner(body);
        const data = await response.json();
        if (response.ok) {
          const newPartner = data.data;
          setPartner(newPartner);
          setPartnerCreated(true);
          addPartner({ key: listKey, item: newPartner });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setPartner,
      setPartnerCreated,
      addPartner,
      listKey,
      setError,
    ]
  );

  return {
    handleCreatePartner,
    error,
    setError,
    processing,
    setProcessing,
    partner,
    setPartner,
    partnerCreated,
    setPartnerCreated,
  };
}

export function useUpdatePartner() {
  const listKey = generateListKey();
  const {
    item: partner,
    setItem: setPartner,
    itemUpdated: partnerUpdated,
    setItemUpdated: setPartnerUpdated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { updatePartner } = usePartnerStore((state) => state);

  const handleUpdatePartner = useCallback(
    async (partner = null, body = null) => {
      try {
        setProcessing(true);
        const response = await PartnerService.updatePartner(partner, body);
        const data = await response.json();
        if (response.ok) {
          const partner = data.data;
          setPartner(partner);
          setPartnerUpdated(true);
          updatePartner({ key: listKey, item: partner });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setPartner,
      setPartnerUpdated,
      updatePartner,
      listKey,
      setError,
    ]
  );

  return {
    handleUpdatePartner,
    error,
    setError,
    processing,
    setProcessing,
    partner,
    setPartner,
    partnerUpdated,
    setPartnerUpdated,
  };
}

export function useRemovePartner() {
  const listKey = generateListKey();
  const {
    item: partner,
    setItem: setPartner,
    itemRemoved: partnerRemoved,
    setItemRemoved: setPartnerRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removePartner } = usePartnerStore((state) => state);

  const handleRemovePartner = useCallback(
    async (partner = null) => {
      try {
        setProcessing(true);
        const response = await PartnerService.removePartner(partner);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setPartner(partner);
            setPartnerRemoved(true);
            removePartner({ key: listKey, item: partner });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setPartner,
      setPartnerRemoved,
      removePartner,
      listKey,
      setError,
    ]
  );

  return {
    handleRemovePartner,
    error,
    setError,
    processing,
    setProcessing,
    partner,
    setPartner,
    partnerRemoved,
    setPartnerRemoved,
  };
}

export function useResetPartners() {
  const listKey = generateListKey();
  const { resetPartnerPage: resetPage, clearPartners } = usePartnerStore(
    (state) => state
  );

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetPartners = useCallback(() => {
    handleResetPage();
    clearPartners({ key: listKey });
  }, [handleResetPage, clearPartners, listKey]);

  return handleResetPartners;
}
