import { useCallback, useEffect } from 'react';

import { WeightDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useWeightDataStore } from 'src/stores/weight-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListWeightDataList() {
  const listKey = generateListKey();
  const {
    weightDataList: _weightDataList,
    weightDataPage: __page,
    setWeightDataPage: _setPage,
    weightDataEndPaging: __endPaging,
    setWeightDataEndPaging: _setEndPaging,
    resetWeightDataPage: resetPage,
    listWeightDataList,
  } = useWeightDataStore((state) => state);
  const { error, setError } = useItem();
  const weightDataList = _weightDataList
    ? _weightDataList[listKey]
    : _weightDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!weightDataList?.length) {
      (async () => {
        await handleListWeightDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [weightDataList?.length]);

  useEffect(() => {
    if (weightDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListWeightDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListWeightDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await WeightDataService.listWeightDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const weightDataList = data.data;
          if (weightDataList?.length || page === 1) {
            listWeightDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listWeightDataList,
      setError,
    ]
  );

  return {
    handleListWeightDataList,
    weightDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetWeightData({
  weightData: _weightData = null,
  ignoreLoadOnMount = false,
}) {
  const { weightData, setWeightData } = useWeightDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetWeightData(_weightData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetWeightData = useCallback(
    async (weightData = null) => {
      try {
        const response = await WeightDataService.getWeightData(weightData);
        const data = await response.json();
        if (response.ok) {
          const weightData = data.data;
          setWeightData(weightData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setWeightData, setError]
  );

  return {
    handleGetWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
  };
}

export function useFindWeightData({
  weightData: _weightData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    weightDataList: _weightDataList,
    setWeightData,
    weightData,
  } = useWeightDataStore((state) => state);
  const weightDataList = _weightDataList
    ? _weightDataList[listKey]
    : _weightDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetWeightData } = useGetWeightData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindWeightData(_weightData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindWeightData = useCallback(
    async (weightData = null) => {
      try {
        if (weightDataList?.length) {
          setWeightData(
            weightDataList.find((item) => item.id === weightData?.id)
          );
        } else {
          await handleGetWeightData(weightData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [weightDataList, setWeightData, handleGetWeightData, setError]
  );

  return {
    handleFindWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
  };
}

export function useRemoveWeightData() {
  const listKey = generateListKey();
  const {
    item: weightData,
    setItem: setWeightData,
    itemRemoved: weightDataRemoved,
    setItemRemoved: setWeightDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeWeightData } = useWeightDataStore((state) => state);

  const handleRemoveWeightData = useCallback(
    async (weightData = null) => {
      try {
        setProcessing(true);
        const response = await WeightDataService.removeWeightData(weightData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setWeightData(weightData);
            setWeightDataRemoved(true);
            removeWeightData({
              key: listKey,
              item: weightData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setWeightData,
      setWeightDataRemoved,
      removeWeightData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
    setWeightData,
    weightDataRemoved,
    setWeightDataRemoved,
  };
}

export function useResetWeightDataList() {
  const listKey = generateListKey();
  const { resetWeightDataPage: resetPage, clearWeightDataList } =
    useWeightDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetWeightDataList = useCallback(() => {
    handleResetPage();
    clearWeightDataList({ key: listKey });
  }, [handleResetPage, clearWeightDataList, listKey]);

  return handleResetWeightDataList;
}
