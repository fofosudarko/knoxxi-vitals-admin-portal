import { useCallback, useEffect } from 'react';

import { GlucoseDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useGlucoseDataStore } from 'src/stores/glucose-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListGlucoseDataList() {
  const listKey = generateListKey();
  const {
    glucoseDataList: _glucoseDataList,
    glucoseDataPage: __page,
    setGlucoseDataPage: _setPage,
    glucoseDataEndPaging: __endPaging,
    setGlucoseDataEndPaging: _setEndPaging,
    resetGlucoseDataPage: resetPage,
    listGlucoseDataList,
  } = useGlucoseDataStore((state) => state);
  const { error, setError } = useItem();
  const glucoseDataList = _glucoseDataList
    ? _glucoseDataList[listKey]
    : _glucoseDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!glucoseDataList?.length) {
      (async () => {
        await handleListGlucoseDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [glucoseDataList?.length]);

  useEffect(() => {
    if (glucoseDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListGlucoseDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListGlucoseDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await GlucoseDataService.listGlucoseDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const glucoseDataList = data.data;
          if (glucoseDataList?.length || page === 1) {
            listGlucoseDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listGlucoseDataList,
      setError,
    ]
  );

  return {
    handleListGlucoseDataList,
    glucoseDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetGlucoseData({
  glucoseData: _glucoseData = null,
  ignoreLoadOnMount = false,
}) {
  const { glucoseData, setGlucoseData } = useGlucoseDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetGlucoseData(_glucoseData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetGlucoseData = useCallback(
    async (glucoseData = null) => {
      try {
        const response = await GlucoseDataService.getGlucoseData(glucoseData);
        const data = await response.json();
        if (response.ok) {
          const glucoseData = data.data;
          setGlucoseData(glucoseData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setGlucoseData, setError]
  );

  return {
    handleGetGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
  };
}

export function useFindGlucoseData({
  glucoseData: _glucoseData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    glucoseDataList: _glucoseDataList,
    setGlucoseData,
    glucoseData,
  } = useGlucoseDataStore((state) => state);
  const glucoseDataList = _glucoseDataList
    ? _glucoseDataList[listKey]
    : _glucoseDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetGlucoseData } = useGetGlucoseData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindGlucoseData(_glucoseData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindGlucoseData = useCallback(
    async (glucoseData = null) => {
      try {
        if (glucoseDataList?.length) {
          setGlucoseData(
            glucoseDataList.find((item) => item.id === glucoseData?.id)
          );
        } else {
          await handleGetGlucoseData(glucoseData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [glucoseDataList, setGlucoseData, handleGetGlucoseData, setError]
  );

  return {
    handleFindGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
  };
}

export function useRemoveGlucoseData() {
  const listKey = generateListKey();
  const {
    item: glucoseData,
    setItem: setGlucoseData,
    itemRemoved: glucoseDataRemoved,
    setItemRemoved: setGlucoseDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeGlucoseData } = useGlucoseDataStore((state) => state);

  const handleRemoveGlucoseData = useCallback(
    async (glucoseData = null) => {
      try {
        setProcessing(true);
        const response = await GlucoseDataService.removeGlucoseData(
          glucoseData
        );
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setGlucoseData(glucoseData);
            setGlucoseDataRemoved(true);
            removeGlucoseData({
              key: listKey,
              item: glucoseData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setGlucoseData,
      setGlucoseDataRemoved,
      removeGlucoseData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
    setGlucoseData,
    glucoseDataRemoved,
    setGlucoseDataRemoved,
  };
}

export function useResetGlucoseDataList() {
  const listKey = generateListKey();
  const { resetGlucoseDataPage: resetPage, clearGlucoseDataList } =
    useGlucoseDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetGlucoseDataList = useCallback(() => {
    handleResetPage();
    clearGlucoseDataList({ key: listKey });
  }, [handleResetPage, clearGlucoseDataList, listKey]);

  return handleResetGlucoseDataList;
}
