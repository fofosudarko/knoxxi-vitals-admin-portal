import { useCallback, useEffect } from 'react';

import { TemperatureDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useTemperatureDataStore } from 'src/stores/temperature-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListTemperatureDataList() {
  const listKey = generateListKey();
  const {
    temperatureDataList: _temperatureDataList,
    temperatureDataPage: __page,
    setTemperatureDataPage: _setPage,
    temperatureDataEndPaging: __endPaging,
    setTemperatureDataEndPaging: _setEndPaging,
    resetTemperatureDataPage: resetPage,
    listTemperatureDataList,
  } = useTemperatureDataStore((state) => state);
  const { error, setError } = useItem();
  const temperatureDataList = _temperatureDataList
    ? _temperatureDataList[listKey]
    : _temperatureDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!temperatureDataList?.length) {
      (async () => {
        await handleListTemperatureDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [temperatureDataList?.length]);

  useEffect(() => {
    if (temperatureDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListTemperatureDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListTemperatureDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await TemperatureDataService.listTemperatureDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const temperatureDataList = data.data;
          if (temperatureDataList?.length || page === 1) {
            listTemperatureDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listTemperatureDataList,
      setError,
    ]
  );

  return {
    handleListTemperatureDataList,
    temperatureDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetTemperatureData({
  temperatureData: _temperatureData = null,
  ignoreLoadOnMount = false,
}) {
  const { temperatureData, setTemperatureData } = useTemperatureDataStore(
    (state) => state
  );
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetTemperatureData(_temperatureData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetTemperatureData = useCallback(
    async (temperatureData = null) => {
      try {
        const response = await TemperatureDataService.getTemperatureData(
          temperatureData
        );
        const data = await response.json();
        if (response.ok) {
          const temperatureData = data.data;
          setTemperatureData(temperatureData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setTemperatureData, setError]
  );

  return {
    handleGetTemperatureData,
    error,
    setError,
    processing,
    setProcessing,
    temperatureData,
  };
}

export function useFindTemperatureData({
  temperatureData: _temperatureData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    temperatureDataList: _temperatureDataList,
    setTemperatureData,
    temperatureData,
  } = useTemperatureDataStore((state) => state);
  const temperatureDataList = _temperatureDataList
    ? _temperatureDataList[listKey]
    : _temperatureDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetTemperatureData } = useGetTemperatureData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindTemperatureData(_temperatureData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindTemperatureData = useCallback(
    async (temperatureData = null) => {
      try {
        if (temperatureDataList?.length) {
          setTemperatureData(
            temperatureDataList.find((item) => item.id === temperatureData?.id)
          );
        } else {
          await handleGetTemperatureData(temperatureData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      temperatureDataList,
      setTemperatureData,
      handleGetTemperatureData,
      setError,
    ]
  );

  return {
    handleFindTemperatureData,
    error,
    setError,
    processing,
    setProcessing,
    temperatureData,
  };
}

export function useRemoveTemperatureData() {
  const listKey = generateListKey();
  const {
    item: temperatureData,
    setItem: setTemperatureData,
    itemRemoved: temperatureDataRemoved,
    setItemRemoved: setTemperatureDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeTemperatureData } = useTemperatureDataStore((state) => state);

  const handleRemoveTemperatureData = useCallback(
    async (temperatureData = null) => {
      try {
        setProcessing(true);
        const response = await TemperatureDataService.removeTemperatureData(
          temperatureData
        );
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setTemperatureData(temperatureData);
            setTemperatureDataRemoved(true);
            removeTemperatureData({
              key: listKey,
              item: temperatureData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setTemperatureData,
      setTemperatureDataRemoved,
      removeTemperatureData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveTemperatureData,
    error,
    setError,
    processing,
    setProcessing,
    temperatureData,
    setTemperatureData,
    temperatureDataRemoved,
    setTemperatureDataRemoved,
  };
}

export function useResetTemperatureDataList() {
  const listKey = generateListKey();
  const { resetTemperatureDataPage: resetPage, clearTemperatureDataList } =
    useTemperatureDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetTemperatureDataList = useCallback(() => {
    handleResetPage();
    clearTemperatureDataList({ key: listKey });
  }, [handleResetPage, clearTemperatureDataList, listKey]);

  return handleResetTemperatureDataList;
}
