import { useCallback, useReducer, useState } from 'react';
import { produce } from 'immer';

import {
  PartnerService,
  UserDataService,
  BPDataService,
  GlucoseDataService,
  TemperatureDataService,
  WeightDataService,
  WaterDataService,
  OxygenDataService,
  CholesterolDataService,
} from 'src/api/app/health';
import { DEFAULT_SELECT_SIZE } from 'src/config';

const initialState = {
  partners: null,
  userDataList: null,
  bpDataList: null,
  glucoseDataList: null,
  temperatureDataList: null,
  weightDataList: null,
  waterDataList: null,
  oxygenDataList: null,
  cholesterolDataList: null,
  error: null,
  processing: false,
};

const types = {
  SET_PARTNERS: 'SET_PARTNERS',
  SET_USER_DATA_LIST: 'SET_USER_DATA_LIST',
  SET_BP_DATA_LIST: 'SET_BP_DATA_LIST',
  SET_GLUCOSE_DATA_LIST: 'SET_GLUCOSE_DATA_LIST',
  SET_TEMPERATURE_DATA_LIST: 'SET_TEMPERATURE_DATA_LIST',
  SET_WEIGHT_DATA_LIST: 'SET_WEIGHT_DATA_LIST',
  SET_WATER_DATA_LIST: 'SET_WATER_DATA_LIST',
  SET_OXYGEN_DATA_LIST: 'SET_OXYGEN_DATA_LIST',
  SET_CHOLESTEROL_DATA_LIST: 'SET_CHOLESTEROL_DATA_LIST',
  CLEAR_SELECT_STATE: 'CLEAR_SELECT_STATE',
  SET_ERROR: 'SET_ERROR',
  SET_PROCESSING: 'SET_PROCESSING',
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_PARTNERS:
      draft.partners = action.payload;
      return;
    case types.SET_USER_DATA_LIST:
      draft.userDataList = action.payload;
      return;
    case types.SET_BP_DATA_LIST:
      draft.bpDataList = action.payload;
      return;
    case types.SET_GLUCOSE_DATA_LIST:
      draft.glucoseDataList = action.payload;
      return;
    case types.SET_TEMPERATURE_DATA_LIST:
      draft.temperatureDataList = action.payload;
      return;
    case types.SET_WEIGHT_DATA_LIST:
      draft.weightDataList = action.payload;
      return;
    case types.SET_WATER_DATA_LIST:
      draft.waterDataList = action.payload;
      return;
    case types.SET_OXYGEN_DATA_LIST:
      draft.oxygenDataList = action.payload;
      return;
    case types.SET_CHOLESTEROL_DATA_LIST:
      draft.cholesterolDataList = action.payload;
      return;
    case types.SET_ERROR:
      draft.error = action.payload;
      return;
    case types.SET_PROCESSING:
      draft.processing = action.payload;
      return;
    case types.CLEAR_SELECT_STATE:
      return initialState;
    default:
      return draft;
  }
});

export function useSelectItems() {
  const [
    {
      error,
      processing,
      partners,
      userDataList,
      bpDataList,
      glucoseDataList,
      temperatureDataList,
      weightDataList,
      waterDataList,
      oxygenDataList,
      cholesterolDataList,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const setError = useCallback((error) => {
    dispatch({ type: types.SET_ERROR, payload: error });
  }, []);
  const setProcessing = useCallback((processing) => {
    dispatch({ type: types.SET_PROCESSING, payload: processing });
  }, []);
  const setPartners = useCallback((partners) => {
    dispatch({ type: types.SET_PARTNERS, payload: partners });
  }, []);
  const setUserDataList = useCallback((userDataList) => {
    dispatch({ type: types.SET_USER_DATA_LIST, payload: userDataList });
  }, []);
  const setBPDataList = useCallback((bpDataList) => {
    dispatch({ type: types.SET_BP_DATA_LIST, payload: bpDataList });
  }, []);
  const setGlucoseDataList = useCallback((glucoseDataList) => {
    dispatch({ type: types.SET_GLUCOSE_DATA_LIST, payload: glucoseDataList });
  }, []);
  const setTemperatureDataList = useCallback((temperatureDataList) => {
    dispatch({
      type: types.SET_TEMPERATURE_DATA_LIST,
      payload: temperatureDataList,
    });
  }, []);
  const setWeightDataList = useCallback((weightDataList) => {
    dispatch({
      type: types.SET_WEIGHT_DATA_LIST,
      payload: weightDataList,
    });
  }, []);
  const setWaterDataList = useCallback((waterDataList) => {
    dispatch({
      type: types.SET_WATER_DATA_LIST,
      payload: waterDataList,
    });
  }, []);
  const setOxygenDataList = useCallback((oxygenDataList) => {
    dispatch({
      type: types.SET_OXYGEN_DATA_LIST,
      payload: oxygenDataList,
    });
  }, []);
  const setCholesterolDataList = useCallback((cholesterolDataList) => {
    dispatch({
      type: types.SET_CHOLESTEROL_DATA_LIST,
      payload: cholesterolDataList,
    });
  }, []);

  const handleListPartners = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { name, size = DEFAULT_SELECT_SIZE } = options;
        const response = await PartnerService.listPartners({
          name,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const partners = data.data;
          setPartners(partners);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setPartners, setError]
  );
  const handleListUserDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { fullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await UserDataService.listUserDataList({
          fullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const userDataList = data.data;
          setUserDataList(userDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setUserDataList, setError]
  );
  const handleListBPDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await BPDataService.listBPDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const bpDataList = data.data;
          setBPDataList(bpDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setBPDataList, setError]
  );
  const handleListGlucoseDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await GlucoseDataService.listGlucoseDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const glucoseDataList = data.data;
          setGlucoseDataList(glucoseDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setGlucoseDataList, setError]
  );
  const handleListTemperatureDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await TemperatureDataService.listTemperatureDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const temperatureDataList = data.data;
          setTemperatureDataList(temperatureDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setTemperatureDataList, setError]
  );
  const handleListWeightDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await WeightDataService.listWeightDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const weightDataList = data.data;
          setWeightDataList(weightDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setWeightDataList, setError]
  );
  const handleListWaterDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await WaterDataService.listWaterDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const waterDataList = data.data;
          setWaterDataList(waterDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setWaterDataList, setError]
  );
  const handleListOxygenDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await OxygenDataService.listOxygenDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const oxygenDataList = data.data;
          setOxygenDataList(oxygenDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setOxygenDataList, setError]
  );
  const handleListCholesterolDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await CholesterolDataService.listCholesterolDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const cholesterolDataList = data.data;
          setCholesterolDataList(cholesterolDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setCholesterolDataList, setError]
  );

  return {
    handleListPartners,
    partners,
    setPartners,
    handleListUserDataList,
    userDataList,
    setUserDataList,
    handleListBPDataList,
    bpDataList,
    setBPDataList,
    handleListGlucoseDataList,
    glucoseDataList,
    setGlucoseDataList,
    handleListTemperatureDataList,
    temperatureDataList,
    setTemperatureDataList,
    handleListWeightDataList,
    weightDataList,
    setWeightDataList,
    handleListWaterDataList,
    waterDataList,
    setWaterDataList,
    handleListOxygenDataList,
    oxygenDataList,
    setOxygenDataList,
    handleListCholesterolDataList,
    cholesterolDataList,
    setCholesterolDataList,
    error,
    setError,
    processing,
    setProcessing,
  };
}

export function useSearchItems() {
  const [isSearch, setIsSearch] = useState(false);
  const handleSearch = useCallback((isSearch) => {
    setIsSearch(isSearch);
  }, []);
  return { isSearch, setIsSearch, handleSearch };
}
