import { useCallback, useReducer } from 'react';
import { produce } from 'immer';

import { EnumService } from 'src/api/app/health';

const initialState = {
  dataChannels: null,
  error: null,
};

const types = {
  SET_DATA_CHANNEL: 'SET_DATA_CHANNEL',
  CLEAR_ENUM_STATE: 'CLEAR_ENUM_STATE',
  SET_ERROR: 'SET_ERROR',
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_DATA_CHANNEL:
      draft.dataChannels = action.payload;
      return;
    case types.SET_ERROR:
      draft.error = action.payload;
      return;
    case types.CLEAR_ENUM_STATE:
      return initialState;
    default:
      return draft;
  }
});

export function useListEnums() {
  const [{ error, dataChannels }, dispatch] = useReducer(reducer, initialState);

  const setDataChannel = useCallback((dataChannels) => {
    dispatch({
      type: types.SET_DATA_CHANNEL,
      payload: dataChannels,
    });
  }, []);
  const setError = useCallback((error) => {
    dispatch({ type: types.SET_ERROR, payload: error });
  }, []);

  const handleListDataChannel = useCallback(async () => {
    try {
      const response = await EnumService.listDataChannel();
      const data = await response.json();
      if (response.ok) {
        setDataChannel(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setDataChannel]);

  return {
    handleListDataChannel,
    dataChannels,
    setDataChannel,
    error,
    setError,
  };
}
