import { useCallback, useEffect } from 'react';

import { OxygenDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useOxygenDataStore } from 'src/stores/oxygen-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListOxygenDataList() {
  const listKey = generateListKey();
  const {
    oxygenDataList: _oxygenDataList,
    oxygenDataPage: __page,
    setOxygenDataPage: _setPage,
    oxygenDataEndPaging: __endPaging,
    setOxygenDataEndPaging: _setEndPaging,
    resetOxygenDataPage: resetPage,
    listOxygenDataList,
  } = useOxygenDataStore((state) => state);
  const { error, setError } = useItem();
  const oxygenDataList = _oxygenDataList
    ? _oxygenDataList[listKey]
    : _oxygenDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!oxygenDataList?.length) {
      (async () => {
        await handleListOxygenDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [oxygenDataList?.length]);

  useEffect(() => {
    if (oxygenDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListOxygenDataList({
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListOxygenDataList = useCallback(
    async ({ page = null }) => {
      try {
        const response = await OxygenDataService.listOxygenDataList({
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const oxygenDataList = data.data;
          if (oxygenDataList?.length || page === 1) {
            listOxygenDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listOxygenDataList,
      setError,
    ]
  );

  return {
    handleListOxygenDataList,
    oxygenDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetOxygenData({
  oxygenData: _oxygenData = null,
  ignoreLoadOnMount = false,
}) {
  const { oxygenData, setOxygenData } = useOxygenDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetOxygenData(_oxygenData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetOxygenData = useCallback(
    async (oxygenData = null) => {
      try {
        const response = await OxygenDataService.getOxygenData(oxygenData);
        const data = await response.json();
        if (response.ok) {
          const oxygenData = data.data;
          setOxygenData(oxygenData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setOxygenData, setError]
  );

  return {
    handleGetOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
  };
}

export function useFindOxygenData({
  oxygenData: _oxygenData = null,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey();
  const {
    oxygenDataList: _oxygenDataList,
    setOxygenData,
    oxygenData,
  } = useOxygenDataStore((state) => state);
  const oxygenDataList = _oxygenDataList
    ? _oxygenDataList[listKey]
    : _oxygenDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetOxygenData } = useGetOxygenData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindOxygenData(_oxygenData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindOxygenData = useCallback(
    async (oxygenData = null) => {
      try {
        if (oxygenDataList?.length) {
          setOxygenData(
            oxygenDataList.find((item) => item.id === oxygenData?.id)
          );
        } else {
          await handleGetOxygenData(oxygenData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [oxygenDataList, setOxygenData, handleGetOxygenData, setError]
  );

  return {
    handleFindOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
  };
}

export function useRemoveOxygenData() {
  const listKey = generateListKey();
  const {
    item: oxygenData,
    setItem: setOxygenData,
    itemRemoved: oxygenDataRemoved,
    setItemRemoved: setOxygenDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeOxygenData } = useOxygenDataStore((state) => state);

  const handleRemoveOxygenData = useCallback(
    async (oxygenData = null) => {
      try {
        setProcessing(true);
        const response = await OxygenDataService.removeOxygenData(oxygenData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setOxygenData(oxygenData);
            setOxygenDataRemoved(true);
            removeOxygenData({
              key: listKey,
              item: oxygenData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setOxygenData,
      setOxygenDataRemoved,
      removeOxygenData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
    setOxygenData,
    oxygenDataRemoved,
    setOxygenDataRemoved,
  };
}

export function useResetOxygenDataList() {
  const listKey = generateListKey();
  const { resetOxygenDataPage: resetPage, clearOxygenDataList } =
    useOxygenDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetOxygenDataList = useCallback(() => {
    handleResetPage();
    clearOxygenDataList({ key: listKey });
  }, [handleResetPage, clearOxygenDataList, listKey]);

  return handleResetOxygenDataList;
}
