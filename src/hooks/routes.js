import { useCallback } from 'react';
import { useRouter } from 'next/router';
import { useAuthStore } from 'src/stores/auth';

const routes = {
  INDEX_ROUTE: '/',
  SIGNIN_ROUTE: '/auth/signin',
  SIGNOUT_ROUTE: '/auth/signout',
  HOME_ROUTE: '/home',
  HEALTH_ROUTE: '/health',
  PARTNERS_ROUTE: '/partners',
  PARTNER_NEW_ROUTE: '/partners/new',
  PARTNER_EDIT_ROUTE: '/partners/{{PARTNER_ID}}/edit',
  USER_DATA_LIST_ROUTE: '/user-data',
  BP_DATA_LIST_ROUTE: '/bp-data',
  GLUCOSE_DATA_LIST_ROUTE: '/glucose-data',
  TEMPERATURE_DATA_LIST_ROUTE: '/temperature-data',
  WEIGHT_DATA_LIST_ROUTE: '/weight-data',
  WATER_DATA_LIST_ROUTE: '/water-data',
  OXYGEN_DATA_LIST_ROUTE: '/oxygen-data',
  CHOLESTEROL_DATA_LIST_ROUTE: '/cholesterol-data',
};

export default function useRoutes() {
  const router = useRouter();
  const appUser = useAuthStore((state) => state.appUser);
  const { account = null } = appUser ?? {};
  const username = account?.username;

  // handle back
  const handleBack = useCallback(() => {
    router.back();
  }, [router]);
  // handle reload
  const handleReload = useCallback(() => {
    router.reload();
  }, [router]);
  // use index route
  const useIndexRoute = useCallback(() => {
    const indexRoute = routes.INDEX_ROUTE;

    const handleIndexRoute = (options = {}) => {
      const { query = null } = options;
      router.push({ pathname: indexRoute, query });
    };

    return { indexRoute, handleIndexRoute };
  }, [router]);
  // use sign in route
  const useSignInRoute = useCallback(() => {
    const signInRoute = routes.SIGNIN_ROUTE;

    const handleSignInRoute = (options = {}) => {
      const { query = null } = options;
      router.push({ pathname: signInRoute, query });
    };

    return { signInRoute, handleSignInRoute };
  }, [router]);
  // use sign out route
  const useSignOutRoute = useCallback(() => {
    const signOutRoute = routes.SIGNOUT_ROUTE;

    const handleSignOutRoute = (options = {}) => {
      const { query = null } = options;
      router.replace({ pathname: signOutRoute, query });
    };

    return { signOutRoute, handleSignOutRoute };
  }, [router]);
  // use home route
  const useHomeRoute = useCallback(() => {
    const homeRoute = handleNamedRoute({
      route: routes.HOME_ROUTE,
      username,
    });

    const handleHomeRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: handleNamedRoute({
          route: homeRoute,
        }),
        query,
      });
    };

    return { homeRoute, handleHomeRoute };
  }, [router, username]);
  // use health route
  const useHealthRoute = useCallback(() => {
    const healthRoute = handleNamedRoute({
      route: routes.HEALTH_ROUTE,
      username,
    });

    const handleHealthRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: handleNamedRoute({
          route: healthRoute,
        }),
        query,
      });
    };

    return { healthRoute, handleHealthRoute };
  }, [router, username]);
  // use partners route
  const usePartnersRoute = useCallback(() => {
    const partnersRoute = handleNamedRoute({
      route: routes.PARTNERS_ROUTE,
    });

    const handlePartnersRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: partnersRoute,
        query,
      });
    };

    return { partnersRoute, handlePartnersRoute };
  }, [router]);
  // use partner new route
  const usePartnerNewRoute = useCallback(() => {
    const partnerNewRoute = handleNamedRoute({
      route: routes.PARTNER_NEW_ROUTE,
    });

    const handlePartnerNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: partnerNewRoute,
        query,
      });
    };

    return { partnerNewRoute, handlePartnerNewRoute };
  }, [router]);
  // use partner edit route
  const usePartnerEditRoute = useCallback(
    (partner = null) => {
      const partnerEditRoute = handleNamedRoute({
        route: routes.PARTNER_EDIT_ROUTE,
        partner,
      });

      const handlePartnerEditRoute = (options = {}) => {
        const { query = null } = options;
        router.push({
          pathname: partnerEditRoute,
          query,
        });
      };

      return { partnerEditRoute, handlePartnerEditRoute };
    },
    [router]
  );
  // use user data list route
  const useUserDataListRoute = useCallback(() => {
    const userDataListRoute = handleNamedRoute({
      route: routes.USER_DATA_LIST_ROUTE,
    });

    const handleUserDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: userDataListRoute,
        query,
      });
    };

    return { userDataListRoute, handleUserDataListRoute };
  }, [router]);
  // use bp data list route
  const useBPDataListRoute = useCallback(() => {
    const bpDataListRoute = handleNamedRoute({
      route: routes.BP_DATA_LIST_ROUTE,
    });

    const handleBPDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: bpDataListRoute,
        query,
      });
    };

    return { bpDataListRoute, handleBPDataListRoute };
  }, [router]);
  // use glucose data list route
  const useGlucoseDataListRoute = useCallback(() => {
    const glucoseDataListRoute = handleNamedRoute({
      route: routes.GLUCOSE_DATA_LIST_ROUTE,
    });

    const handleGlucoseDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: glucoseDataListRoute,
        query,
      });
    };

    return { glucoseDataListRoute, handleGlucoseDataListRoute };
  }, [router]);
  // use temperature data list route
  const useTemperatureDataListRoute = useCallback(() => {
    const temperatureDataListRoute = handleNamedRoute({
      route: routes.TEMPERATURE_DATA_LIST_ROUTE,
    });

    const handleTemperatureDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: temperatureDataListRoute,
        query,
      });
    };

    return { temperatureDataListRoute, handleTemperatureDataListRoute };
  }, [router]);
  // use weight data list route
  const useWeightDataListRoute = useCallback(() => {
    const weightDataListRoute = handleNamedRoute({
      route: routes.WEIGHT_DATA_LIST_ROUTE,
    });

    const handleWeightDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: weightDataListRoute,
        query,
      });
    };

    return { weightDataListRoute, handleWeightDataListRoute };
  }, [router]);
  // use water data list route
  const useWaterDataListRoute = useCallback(() => {
    const waterDataListRoute = handleNamedRoute({
      route: routes.WATER_DATA_LIST_ROUTE,
    });

    const handleWaterDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: waterDataListRoute,
        query,
      });
    };

    return { waterDataListRoute, handleWaterDataListRoute };
  }, [router]);
  // use oxygen data list route
  const useOxygenDataListRoute = useCallback(() => {
    const oxygenDataListRoute = handleNamedRoute({
      route: routes.OXYGEN_DATA_LIST_ROUTE,
    });

    const handleOxygenDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: oxygenDataListRoute,
        query,
      });
    };

    return { oxygenDataListRoute, handleOxygenDataListRoute };
  }, [router]);
  // use cholesterol data list route
  const useCholesterolDataListRoute = useCallback(() => {
    const cholesterolDataListRoute = handleNamedRoute({
      route: routes.CHOLESTEROL_DATA_LIST_ROUTE,
    });

    const handleCholesterolDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: cholesterolDataListRoute,
        query,
      });
    };

    return { cholesterolDataListRoute, handleCholesterolDataListRoute };
  }, [router]);

  return {
    appUser,
    router,
    handleBack,
    handleReload,
    useSignInRoute,
    useSignOutRoute,
    useIndexRoute,
    useHomeRoute,
    useHealthRoute,
    usePartnersRoute,
    usePartnerNewRoute,
    usePartnerEditRoute,
    useUserDataListRoute,
    useBPDataListRoute,
    useGlucoseDataListRoute,
    useTemperatureDataListRoute,
    useWeightDataListRoute,
    useWaterDataListRoute,
    useOxygenDataListRoute,
    useCholesterolDataListRoute,
    routes,
  };
}

function handleNamedRoute({ route, username, partner = null }) {
  route = route || '';

  const { id: partnerId } = partner ?? {};

  if (username) {
    route = route.replace('{{APP_USERNAME}}', username);
  }
  if (partnerId) {
    route = route.replace('{{PARTNER_ID}}', partnerId);
  }

  return route;
}
