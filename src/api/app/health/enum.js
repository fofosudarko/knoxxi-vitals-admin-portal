import { fetchHealthResource } from '../../fetch/health';

class EnumService {
  static async listDataChannel() {
    return await fetchHealthResource(`vitals/enums/data-channel`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });
  }
}

export default EnumService;
