import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class OxygenDataService {
  static async listOxygenDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/oxygen-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getOxygenData(oxygenData = null) {
    const oxygenDataId = oxygenData?.id;
    return await fetchHealthResource(`vitals/oxygen-data/${oxygenDataId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeOxygenData(oxygenData = null) {
    const oxygenDataId = oxygenData?.id;
    return await fetchHealthResource(`vitals/oxygen-data/${oxygenDataId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
