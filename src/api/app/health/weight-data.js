import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class WeightDataService {
  static async listWeightDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/weight-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getWeightData(weightData = null) {
    const weightDataId = weightData?.id;
    return await fetchHealthResource(`vitals/weight-data/${weightDataId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeWeightData(weightData = null) {
    const weightDataId = weightData?.id;
    return await fetchHealthResource(`vitals/weight-data/${weightDataId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
