import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class PartnerService {
  static async listPartners({
    name,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/partners${generateQuerystring({
        name,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getPartner(partner = null) {
    const partnerId = partner?.id;
    return await fetchHealthResource(`vitals/partners/${partnerId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async createPartner(body = null) {
    return await fetchHealthResource(`vitals/partners`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async updatePartner(partner = null, body = null) {
    const partnerId = partner?.id;
    return await fetchHealthResource(`vitals/partners/${partnerId}`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removePartner(partner = null) {
    const partnerId = partner?.id;
    return await fetchHealthResource(`vitals/partners/${partnerId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
