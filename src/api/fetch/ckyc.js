import { KNOXXI_CKYC_SERVICE_API_URL } from 'src/config';
import { cleanupResource } from 'src/utils/index';

const headers = new Headers();

// fetch resources from ckyc service api
export default async function fetchCkycResource(resource, options = null) {
  const url = KNOXXI_CKYC_SERVICE_API_URL + '/' + cleanupResource(resource);
  if (options?.headers) {
    const optionsHeaders = options?.headers ?? null;
    for (const key of Object.keys(optionsHeaders)) {
      headers.set(key, optionsHeaders[key]);
    }
    delete options?.headers;
  }
  return await fetch(url, { ...options, headers });
}
