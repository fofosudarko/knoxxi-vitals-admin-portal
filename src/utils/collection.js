/*global Set*/

function isCollection(collection) {
  return Array.isArray(collection);
}

function isObject(item) {
  return typeof item === 'object';
}

export default class Collection {
  static prepend(collection = [], item = {}, options = null) {
    const { propertyFilter = 'id', isUnique = false } = options ?? {};
    if (!isCollection(collection)) {
      return [];
    }
    if (isUnique) {
      const difference = Collection.findDifference([item], collection, {
        propertyFilter,
      });
      collection = [...difference, ...collection];
    } else {
      collection = [item, ...collection];
    }
    return collection;
  }

  static prependAll(collection = [], items = [], options = null) {
    const { propertyFilter = 'id', isUnique = false } = options ?? {};
    if (!isCollection(collection)) {
      return [];
    }
    if (isUnique) {
      const difference = Collection.findDifference(items, collection, {
        propertyFilter,
      });
      collection = [...difference, ...collection];
    } else {
      collection = [...items, ...collection];
    }
    return collection;
  }

  static append(collection = [], item = {}, options = null) {
    const { propertyFilter = 'id', isUnique = false } = options ?? {};
    if (!isCollection(collection)) {
      return [];
    }
    if (isUnique) {
      const difference = Collection.findDifference([item], collection, {
        propertyFilter,
      });
      collection = [...collection, ...difference];
    } else {
      collection = [...collection, item];
    }
    return collection;
  }

  static appendAll(collection = [], items = [], options = null) {
    const { propertyFilter = 'id', isUnique = false } = options ?? {};
    if (!isCollection(collection)) {
      return items;
    }
    if (isUnique) {
      const difference = Collection.findDifference(items, collection, {
        propertyFilter,
      });
      collection = [...collection, ...difference];
    } else {
      collection = [...collection, ...items];
    }
    return collection;
  }

  static update(collection = [], item = {}, options = null) {
    const { propertyFilter = 'id' } = options ?? {};
    if (!isCollection(collection)) {
      return [];
    }
    const itemIndex = collection.findIndex((i) =>
      isObject(item) ? i[propertyFilter] === item[propertyFilter] : i === item
    );
    if (itemIndex === -1) {
      return collection;
    }
    if (Object.isFrozen(collection)) {
      collection = Array.from(collection);
    }
    collection.splice(itemIndex, 1, item);
    return collection;
  }

  static updateAll(collection = [], items = [], options = null) {
    const { propertyFilter = 'id' } = options ?? {};
    if (!isCollection(collection)) {
      return [];
    }
    if (Object.isFrozen(collection)) {
      collection = Array.from(collection);
    }
    for (const item of items) {
      const itemIndex = collection.findIndex((i) =>
        isObject(item) ? i[propertyFilter] === item[propertyFilter] : i === item
      );
      if (itemIndex === -1) {
        continue;
      }
      collection.splice(itemIndex, 1, item);
    }
    return collection;
  }

  static list(collection = [], items = [], options = null) {
    const { propertyFilter = 'id', isUnique = false } = options ?? {};
    return Collection.appendAll(collection, items, {
      propertyFilter,
      isUnique,
    });
  }

  static upsert(collection = [], item = {}, options = null) {
    const { propertyFilter = 'id', operation = 'PREPEND_ITEM' } = options ?? {};
    if (!isCollection(collection)) {
      return [];
    }
    if (!['PREPEND_ITEM', 'APPEND_ITEM'].includes(operation)) {
      throw new Error(
        'Operation unknown. Must be either PREPEND_ITEM or APPEND_ITEM'
      );
    }
    const itemIndex = collection.findIndex((i) =>
      isObject(item) ? i[propertyFilter] === item[propertyFilter] : i === item
    );
    if (itemIndex === -1) {
      if (operation === 'PREPEND_ITEM') {
        return Collection.prepend(collection, item);
      } else {
        return Collection.append(collection, item);
      }
    }
    if (Object.isFrozen(collection)) {
      collection = Array.from(collection);
    }
    collection.splice(itemIndex, 1, item);
    return collection;
  }

  static remove(collection = [], item = {}, options = null) {
    const { propertyFilter = 'id' } = options ?? {};
    return isCollection(collection) && (item !== null || item !== undefined)
      ? collection.filter((i) =>
          isObject(item)
            ? i[propertyFilter] !== item[propertyFilter]
            : i === item
        )
      : collection;
  }

  static removeAll(collection = [], items = [], options = null) {
    const { propertyFilter = 'id' } = options ?? {};
    return Collection.findDifference(collection, items, propertyFilter);
  }

  static findIntersection(
    firstCollection = null,
    secondCollection = null,
    options = null
  ) {
    const { propertyFilter = 'id' } = options ?? {};
    if (!isCollection(firstCollection) && !isCollection(secondCollection)) {
      return secondCollection;
    } else if (
      !isCollection(secondCollection) &&
      isCollection(firstCollection)
    ) {
      return firstCollection;
    }
    const firstCollectionSet = new Set(
      firstCollection.map((item) =>
        isObject(item) ? item[propertyFilter] : item
      )
    );
    const secondCollectionSet = new Set(
      secondCollection.map((item) =>
        isObject(item) ? item[propertyFilter] : item
      )
    );
    const intersection = [
      ...[...firstCollectionSet].filter((item) =>
        secondCollectionSet.has(item)
      ),
    ];
    return firstCollection.filter((item) =>
      intersection.includes(isObject(item) ? item[propertyFilter] : item)
    );
  }

  static findDifference(
    firstCollection = null,
    secondCollection = null,
    options = null
  ) {
    const { propertyFilter = 'id' } = options ?? {};
    if (!isCollection(firstCollection) && isCollection(secondCollection)) {
      return secondCollection;
    } else if (
      !isCollection(secondCollection) &&
      isCollection(firstCollection)
    ) {
      return firstCollection;
    }
    const firstCollectionSet = new Set(
      firstCollection.map((item) =>
        isObject(item) ? item[propertyFilter] : item
      )
    );
    const secondCollectionSet = new Set(
      secondCollection.map((item) =>
        isObject(item) ? item[propertyFilter] : item
      )
    );
    const difference = [
      ...[...firstCollectionSet].filter(
        (item) => !secondCollectionSet.has(item)
      ),
    ];
    return firstCollection.filter((item) =>
      difference.includes(isObject(item) ? item[propertyFilter] : item)
    );
  }

  static get(collection = [], item = {}, options = null) {
    const { propertyFilter = 'id' } = options ?? {};
    if (!isCollection(collection)) {
      return item;
    }
    return collection.find((i) =>
      isObject(item) ? i[propertyFilter] === item[propertyFilter] : i === item
    );
  }
}
