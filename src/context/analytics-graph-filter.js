import React, { useReducer, useCallback } from 'react';
import PropTypes from 'prop-types';
import { produce } from 'immer';

const AnalyticsGraphFilterContext = React.createContext();

const initialState = {
  date: undefined,
  count: undefined,
  interval: undefined,
  order: undefined,
};
const types = {
  SET_DATE: 'SET_DATE',
  SET_COUNT: 'SET_COUNT',
  SET_INTERVAL: 'SET_INTERVAL',
  SET_ORDER: 'SET_ORDER',
  CLEAR_ANALYTICS_GRAPH_FILTER: 'CLEAR_ANALYTICS_GRAPH_FILTER',
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_DATE:
      draft.date = action.payload;
      return;
    case types.SET_COUNT:
      draft.count = action.payload;
      return;
    case types.SET_INTERVAL:
      draft.interval = action.payload;
      return;
    case types.SET_ORDER:
      draft.order = action.payload;
      return;
    case types.CLEAR_ANALYTICS_GRAPH_FILTER:
      return initialState;
    default:
      return draft;
  }
});

function AnalyticsGraphFilterProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  const setDate = useCallback((date) => {
    dispatch({
      type: types.SET_DATE,
      payload: date,
    });
  }, []);
  const setCount = useCallback((count) => {
    dispatch({
      type: types.SET_COUNT,
      payload: count,
    });
  }, []);
  const setInterval = useCallback((interval) => {
    dispatch({
      type: types.SET_INTERVAL,
      payload: interval,
    });
  }, []);
  const setOrder = useCallback((order) => {
    dispatch({
      type: types.SET_ORDER,
      payload: order,
    });
  }, []);
  const clearAnalyticsGraphFilterState = useCallback(() => {
    dispatch({ type: types.CLEAR_ANALYTICS_GRAPH_FILTER });
  }, []);

  return (
    <AnalyticsGraphFilterContext.Provider
      value={{
        ...state,
        setDate,
        setCount,
        setInterval,
        setOrder,
        clearAnalyticsGraphFilterState,
      }}
    >
      {children}
    </AnalyticsGraphFilterContext.Provider>
  );
}

export { AnalyticsGraphFilterContext, AnalyticsGraphFilterProvider };

AnalyticsGraphFilterProvider.propTypes = {
  children: PropTypes.node,
};
AnalyticsGraphFilterProvider.defaultProps = {
  children: null,
};
