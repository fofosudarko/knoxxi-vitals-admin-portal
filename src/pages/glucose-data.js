import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { GlucoseDataListPage } from 'src/components/glucose-data';

export default function GlucoseDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Glucose data list</title>
      </Head>
      {appUser && hasMounted ? (
        <GlucoseDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

GlucoseDataList.getLayout = getAppLayout;

GlucoseDataList.propTypes = {};
GlucoseDataList.defaultProps = {};
