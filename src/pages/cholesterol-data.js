import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { CholesterolDataListPage } from 'src/components/cholesterol-data';

export default function CholesterolDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Cholesterol data list</title>
      </Head>
      {appUser && hasMounted ? (
        <CholesterolDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

CholesterolDataList.getLayout = getAppLayout;

CholesterolDataList.propTypes = {};
CholesterolDataList.defaultProps = {};
