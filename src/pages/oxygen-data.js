import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { OxygenDataListPage } from 'src/components/oxygen-data';

export default function OxygenDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Oxygen data list</title>
      </Head>
      {appUser && hasMounted ? (
        <OxygenDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

OxygenDataList.getLayout = getAppLayout;

OxygenDataList.propTypes = {};
OxygenDataList.defaultProps = {};
