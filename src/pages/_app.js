import React from 'react';
import PropTypes from 'prop-types';
import nProgress from 'nprogress';
import { Router } from 'next/router';

import 'src/styles/main.scss';
import 'src/styles/nprogress.css';

import { AppProvider, NotificationProvider } from 'src/context';
import { NotificationContainer } from 'src/components/lib';

Router.events.on('routeChangeStart', nProgress.start);
Router.events.on('routeChangeError', nProgress.done);
Router.events.on('routeChangeComplete', nProgress.done);

const Noop = (Component) => <>{Component}</>;

// suppress useLayoutEffect warnings when running outside a browser
if (typeof window !== 'object') {
  React.useLayoutEffect = React.useEffect;
}

function MyApp({ Component, pageProps }) {
  const getLayout = Component.getLayout || Noop;

  return (
    <AppProvider>
      <NotificationProvider>
        <NotificationContainer>
          {getLayout(<Component {...pageProps} />)}
        </NotificationContainer>
      </NotificationProvider>
    </AppProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.func,
  pageProps: PropTypes.object,
};
MyApp.defaultProps = {
  Component: null,
  pageProps: null,
};

export default MyApp;
