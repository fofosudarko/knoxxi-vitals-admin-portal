import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { HealthPage } from 'src/components/init';

export default function HealthIndex() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Health</title>
      </Head>
      {appUser && hasMounted ? (
        <HealthPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

HealthIndex.getLayout = getAppLayout;
