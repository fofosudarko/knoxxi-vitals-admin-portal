import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { UserDataListPage } from 'src/components/user-data';

export default function UserDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>User data list</title>
      </Head>
      {appUser && hasMounted ? (
        <UserDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

UserDataList.getLayout = getAppLayout;

UserDataList.propTypes = {};
UserDataList.defaultProps = {};
