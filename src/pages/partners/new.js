import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { PartnerNewPage } from 'src/components/partners';

export default function PartnerNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New partner</title>
      </Head>
      {appUser && hasMounted ? (
        <PartnerNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

PartnerNew.getLayout = getAppLayout;

PartnerNew.propTypes = {};
PartnerNew.defaultProps = {};
