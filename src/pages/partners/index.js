import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { PartnersPage } from 'src/components/partners';

export default function Partners() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Partners</title>
      </Head>
      {appUser && hasMounted ? (
        <PartnersPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

Partners.getLayout = getAppLayout;

Partners.propTypes = {};
Partners.defaultProps = {};
