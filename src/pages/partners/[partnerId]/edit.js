import PropTypes from 'prop-types';
import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { useGetPartner } from 'src/hooks/api';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { PartnerEditPage } from 'src/components/partners';

export default function PartnerEdit({ partnerId }) {
  const { appUser, hasMounted } = useAuth();
  const { partner } = useGetPartner({
    partner: { id: partnerId },
  });

  return (
    <>
      <Head>
        <title>Edit partner</title>
      </Head>
      {appUser && hasMounted && partner ? (
        <PartnerEditPage appUser={appUser} partner={partner} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

PartnerEdit.getLayout = getAppLayout;

export function getServerSideProps(context) {
  const { partnerId = null } = context.params;
  return { props: { partnerId } };
}

PartnerEdit.propTypes = {
  partnerId: PropTypes.string,
};
PartnerEdit.defaultProps = {
  partnerId: undefined,
};
