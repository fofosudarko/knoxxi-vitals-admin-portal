import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { WeightDataListPage } from 'src/components/weight-data';

export default function WeightDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Weight data list</title>
      </Head>
      {appUser && hasMounted ? (
        <WeightDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

WeightDataList.getLayout = getAppLayout;

WeightDataList.propTypes = {};
WeightDataList.defaultProps = {};
