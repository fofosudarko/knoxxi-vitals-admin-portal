import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { TemperatureDataListPage } from 'src/components/temperature-data';

export default function TemperatureDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Temperature data list</title>
      </Head>
      {appUser && hasMounted ? (
        <TemperatureDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

TemperatureDataList.getLayout = getAppLayout;

TemperatureDataList.propTypes = {};
TemperatureDataList.defaultProps = {};
