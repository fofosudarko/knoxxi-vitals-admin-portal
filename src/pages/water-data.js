import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { WaterDataListPage } from 'src/components/water-data';

export default function WaterDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Water data list</title>
      </Head>
      {appUser && hasMounted ? (
        <WaterDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

WaterDataList.getLayout = getAppLayout;

WaterDataList.propTypes = {};
WaterDataList.defaultProps = {};
