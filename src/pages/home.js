import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { HomePage } from 'src/components/init';

export default function HomeIndex() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      {appUser && hasMounted ? (
        <HomePage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

HomeIndex.getLayout = getAppLayout;
