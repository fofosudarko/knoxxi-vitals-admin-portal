import * as yup from 'yup';

const USERNAME_INPUT_VALIDATION = {
  regex: {
    value: /^[_\w+][_\w\d]+$/,
    errorMessage:
      'Invalid username. Must be alphanumeric characters and underscore.',
  },
  length: { max: 50, min: 5 },
  required: {
    errorMessage: 'Username required',
  },
};

const PASSWORD_INPUT_VALIDATION = {
  regex: {
    value: /^[\W\w\d\D]+$/,
    errorMessage:
      'Invalid password. Must be alphanumeric characters and any special character.',
  },
  length: { max: 20, min: 8 },
  required: {
    errorMessage: 'Password required',
  },
};

const EMAIL_ADDRESS_INPUT_VALIDATION = {
  regex: {
    value: undefined,
    errorMessage: 'Invalid email address',
  },
  required: {
    errorMessage: 'Email address required',
  },
};

const MEDIA_INPUT_VALIDATION = {
  required: {
    errorMessage: 'Media required',
  },
};

const GENERAL_NAME_INPUT_VALIDATION = {
  regex: {
    value: /^[_\w][\w\d\s-'"]+$/,
    errorMessage:
      'Invalid name. Must be alphanumeric characters with either spaces or special characters.',
  },
  length: { max: 100, min: 2 },
  required: {
    errorMessage: 'Name required',
  },
};

const PHONE_NUMBER_INPUT_VALIDATION = {
  regex: {
    value: /^[+]?\d{12,15}$/,
    errorMessage: 'Invalid phone number. Must be at least 12 digits long.',
  },
  length: { max: 15, min: 0 },
  required: {
    errorMessage: 'Mobile number required',
  },
};

const GENERAL_PHONE_NUMBER_INPUT_VALIDATION = {
  regex: {
    value: /^[+]?\d{12,15}$/,
    errorMessage: 'Invalid phone number. Must be at least 12 digits long.',
  },
  length: { max: 15, min: 0 },
  required: {
    errorMessage: 'Mobile number required',
  },
};

// value: /^\d+\s+[\w\d\s]+(,\s+[\w\s]+)+$/,

const CONTENT_NAME_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Content name must be a string',
  },
  required: {
    errorMessage: 'Content name required',
  },
};

const PARTNER_NAME_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Partner name must be a string',
  },
  required: {
    errorMessage: 'Partner name required',
  },
};

const PARTNER_LOCATION_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Partner location must be a string',
  },
};

const PARTNER_ALIAS_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Partner alias must be a string',
  },
  required: {
    errorMessage: 'Partner alias required',
  },
  maxLength: {
    value: 3000,
    errorMessage: 'Partner text length exceeded 3000 characters',
  },
};

export const YUP_USERNAME_VALIDATOR = yup
  .string()
  .max(USERNAME_INPUT_VALIDATION.length.max)
  .min(USERNAME_INPUT_VALIDATION.length.min)
  .matches(
    USERNAME_INPUT_VALIDATION.regex.value,
    USERNAME_INPUT_VALIDATION.regex.errorMessage
  )
  .required(USERNAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PASSWORD_VALIDATOR = yup
  .string()
  .max(PASSWORD_INPUT_VALIDATION.length.max)
  .min(PASSWORD_INPUT_VALIDATION.length.min)
  .matches(
    PASSWORD_INPUT_VALIDATION.regex.value,
    PASSWORD_INPUT_VALIDATION.regex.errorMessage
  )
  .required(PASSWORD_INPUT_VALIDATION.required.errorMessage);

export const YUP_EMAIL_ADDRESS_VALIDATOR = yup
  .string()
  .nullable()
  .email(EMAIL_ADDRESS_INPUT_VALIDATION.regex.errorMessage);

export const YUP_MEDIA_VALIDATOR = yup
  .string()
  .nullable()
  .required(MEDIA_INPUT_VALIDATION.required.errorMessage);

export const YUP_GENERAL_NAME_VALIDATOR = yup
  .string()
  .max(GENERAL_NAME_INPUT_VALIDATION.length.max)
  .min(GENERAL_NAME_INPUT_VALIDATION.length.min)
  .matches(
    GENERAL_NAME_INPUT_VALIDATION.regex.value,
    GENERAL_NAME_INPUT_VALIDATION.regex.errorMessage
  )
  .required(GENERAL_NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PHONE_NUMBER_VALIDATOR = yup
  .string()
  .nullable()
  .required(PHONE_NUMBER_INPUT_VALIDATION.required.errorMessage)
  .max(PHONE_NUMBER_INPUT_VALIDATION.length.max)
  .min(PHONE_NUMBER_INPUT_VALIDATION.length.min)
  .matches(
    PHONE_NUMBER_INPUT_VALIDATION.regex.value,
    PHONE_NUMBER_INPUT_VALIDATION.regex.errorMessage
  );

export const YUP_GENERAL_PHONE_NUMBER_VALIDATOR = yup
  .string()
  .nullable()
  .max(GENERAL_PHONE_NUMBER_INPUT_VALIDATION.length.max)
  .min(GENERAL_PHONE_NUMBER_INPUT_VALIDATION.length.min)
  .matches(
    GENERAL_PHONE_NUMBER_INPUT_VALIDATION.regex.value,
    GENERAL_PHONE_NUMBER_INPUT_VALIDATION.regex.errorMessage
  );

export const YUP_GENERAL_ADDRESS_VALIDATOR = yup.string().nullable();

export const YUP_DATE_OF_BIRTH_VALIDATOR = yup.string();

export const YUP_GENDER_VALIDATOR = yup.string();

export const YUP_POSTAL_ADDRESS_VALIDATOR = yup.string().nullable();

export const YUP_CONTENT_NAME_VALIDATOR = yup
  .string(CONTENT_NAME_INPUT_VALIDATION.string.errorMessage)
  .required(CONTENT_NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PARTNER_NAME_VALIDATOR = yup
  .string(PARTNER_NAME_INPUT_VALIDATION.string.errorMessage)
  .required(PARTNER_NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PARTNER_LOCATION_VALIDATOR = yup.string(
  PARTNER_LOCATION_INPUT_VALIDATION.string.errorMessage
);

export const YUP_PARTNER_ALIAS_VALIDATOR = yup
  .string(PARTNER_ALIAS_INPUT_VALIDATION.string.errorMessage)
  .required(PARTNER_ALIAS_INPUT_VALIDATION.required.errorMessage)
  .max(
    PARTNER_ALIAS_INPUT_VALIDATION.maxLength.value,
    PARTNER_ALIAS_INPUT_VALIDATION.maxLength.errorMessage
  );
