import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  partner: null,
  partners: null,
  partnerPage: null,
  partnerEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listPartners(partners) {
    const key = partners.key,
      items = partners.items;
    set((state) => ({
      partners: {
        ...state.partners,
        [key]: Collection.list(
          state.partners ? state.partners[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setPartner(partner) {
    set({ partner });
  },
  addPartner(partner) {
    const key = partner.key,
      item = partner.item;
    set((state) => ({
      partners: {
        ...state.partners,
        [key]: Collection.prepend(
          state.partners ? state.partners[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  updatePartner(partner) {
    const key = partner.key,
      item = partner.item;
    set((state) => ({
      partners: {
        ...state.partners,
        [key]: Collection.update(
          state.partners ? state.partners[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removePartner(partner) {
    const key = partner.key,
      item = partner.item;
    set((state) => ({
      partners: {
        ...state.partners,
        [key]: Collection.remove(
          state.partners ? state.partners[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setPartnerPage(partnerPage) {
    const key = partnerPage.key,
      item = partnerPage.item;
    set((state) => ({
      partnerPage: {
        ...state.partnerPage,
        [key]: item,
      },
    }));
  },
  setPartnerEndPaging(partnerEndPaging) {
    const key = partnerEndPaging.key,
      item = partnerEndPaging.item;
    set((state) => ({
      partnerEndPaging: {
        ...state.partnerEndPaging,
        [key]: item,
      },
    }));
  },
  resetPartnerPage(partnerPage) {
    const key = partnerPage.key;
    set((state) => ({
      partnerPage: {
        ...state.partnerPage,
        [key]: null,
      },
      partnerEndPaging: {
        ...state.partnerEndPaging,
        [key]: null,
      },
    }));
  },
  clearPartners(partners) {
    const key = partners.key;
    set((state) => ({
      partners: {
        ...state.partners,
        [key]: null,
      },
    }));
  },
  clearPartnerState() {
    set({ ...initialState });
  },
});
export const usePartnerStore = create(storeFn);
