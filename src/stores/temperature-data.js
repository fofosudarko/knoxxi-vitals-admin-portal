import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  temperatureData: null,
  temperatureDataList: null,
  temperatureDataPage: null,
  temperatureDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listTemperatureDataList(temperatureDataList) {
    const key = temperatureDataList.key,
      items = temperatureDataList.items;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: Collection.list(
          state.temperatureDataList ? state.temperatureDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setTemperatureData(temperatureData) {
    set({ temperatureData });
  },
  removeTemperatureData(temperatureData) {
    const key = temperatureData.key,
      item = temperatureData.item;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: Collection.remove(
          state.temperatureDataList ? state.temperatureDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setTemperatureDataPage(temperatureDataPage) {
    const key = temperatureDataPage.key,
      item = temperatureDataPage.item;
    set((state) => ({
      temperatureDataPage: {
        ...state.temperatureDataPage,
        [key]: item,
      },
    }));
  },
  setTemperatureDataEndPaging(temperatureDataEndPaging) {
    const key = temperatureDataEndPaging.key,
      item = temperatureDataEndPaging.item;
    set((state) => ({
      temperatureDataEndPaging: {
        ...state.temperatureDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetTemperatureDataPage(temperatureDataPage) {
    const key = temperatureDataPage.key;
    set((state) => ({
      temperatureDataPage: {
        ...state.temperatureDataPage,
        [key]: null,
      },
      temperatureDataEndPaging: {
        ...state.temperatureDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearTemperatureDataList(temperatureDataList) {
    const key = temperatureDataList.key;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: null,
      },
    }));
  },
  clearTemperatureDataState() {
    set({ ...initialState });
  },
});
export const useTemperatureDataStore = create(storeFn);
