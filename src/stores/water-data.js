import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  waterData: null,
  waterDataList: null,
  waterDataPage: null,
  waterDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listWaterDataList(waterDataList) {
    const key = waterDataList.key,
      items = waterDataList.items;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: Collection.list(
          state.waterDataList ? state.waterDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setWaterData(waterData) {
    set({ waterData });
  },
  removeWaterData(waterData) {
    const key = waterData.key,
      item = waterData.item;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: Collection.remove(
          state.waterDataList ? state.waterDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setWaterDataPage(waterDataPage) {
    const key = waterDataPage.key,
      item = waterDataPage.item;
    set((state) => ({
      waterDataPage: {
        ...state.waterDataPage,
        [key]: item,
      },
    }));
  },
  setWaterDataEndPaging(waterDataEndPaging) {
    const key = waterDataEndPaging.key,
      item = waterDataEndPaging.item;
    set((state) => ({
      waterDataEndPaging: {
        ...state.waterDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetWaterDataPage(waterDataPage) {
    const key = waterDataPage.key;
    set((state) => ({
      waterDataPage: {
        ...state.waterDataPage,
        [key]: null,
      },
      waterDataEndPaging: {
        ...state.waterDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearWaterDataList(waterDataList) {
    const key = waterDataList.key;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: null,
      },
    }));
  },
  clearWaterDataState() {
    set({ ...initialState });
  },
});
export const useWaterDataStore = create(storeFn);
