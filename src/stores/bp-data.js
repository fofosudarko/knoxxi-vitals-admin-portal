import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  bpData: null,
  bpDataList: null,
  bpDataPage: null,
  bpDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listBPDataList(bpDataList) {
    const key = bpDataList.key,
      items = bpDataList.items;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: Collection.list(
          state.bpDataList ? state.bpDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setBPData(bpData) {
    set({ bpData });
  },
  removeBPData(bpData) {
    const key = bpData.key,
      item = bpData.item;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: Collection.remove(
          state.bpDataList ? state.bpDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setBPDataPage(bpDataPage) {
    const key = bpDataPage.key,
      item = bpDataPage.item;
    set((state) => ({
      bpDataPage: {
        ...state.bpDataPage,
        [key]: item,
      },
    }));
  },
  setBPDataEndPaging(bpDataEndPaging) {
    const key = bpDataEndPaging.key,
      item = bpDataEndPaging.item;
    set((state) => ({
      bpDataEndPaging: {
        ...state.bpDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetBPDataPage(bpDataPage) {
    const key = bpDataPage.key;
    set((state) => ({
      bpDataPage: {
        ...state.bpDataPage,
        [key]: null,
      },
      bpDataEndPaging: {
        ...state.bpDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearBPDataList(bpDataList) {
    const key = bpDataList.key;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: null,
      },
    }));
  },
  clearBPDataState() {
    set({ ...initialState });
  },
});
export const useBPDataStore = create(storeFn);
