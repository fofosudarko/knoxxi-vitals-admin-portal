import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  weightData: null,
  weightDataList: null,
  weightDataPage: null,
  weightDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listWeightDataList(weightDataList) {
    const key = weightDataList.key,
      items = weightDataList.items;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: Collection.list(
          state.weightDataList ? state.weightDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setWeightData(weightData) {
    set({ weightData });
  },
  removeWeightData(weightData) {
    const key = weightData.key,
      item = weightData.item;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: Collection.remove(
          state.weightDataList ? state.weightDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setWeightDataPage(weightDataPage) {
    const key = weightDataPage.key,
      item = weightDataPage.item;
    set((state) => ({
      weightDataPage: {
        ...state.weightDataPage,
        [key]: item,
      },
    }));
  },
  setWeightDataEndPaging(weightDataEndPaging) {
    const key = weightDataEndPaging.key,
      item = weightDataEndPaging.item;
    set((state) => ({
      weightDataEndPaging: {
        ...state.weightDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetWeightDataPage(weightDataPage) {
    const key = weightDataPage.key;
    set((state) => ({
      weightDataPage: {
        ...state.weightDataPage,
        [key]: null,
      },
      weightDataEndPaging: {
        ...state.weightDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearWeightDataList(weightDataList) {
    const key = weightDataList.key;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: null,
      },
    }));
  },
  clearWeightDataState() {
    set({ ...initialState });
  },
});
export const useWeightDataStore = create(storeFn);
