import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  userData: null,
  userDataList: null,
  userDataPage: null,
  userDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listUserDataList(userDataList) {
    const key = userDataList.key,
      items = userDataList.items;
    set((state) => ({
      userDataList: {
        ...state.userDataList,
        [key]: Collection.list(
          state.userDataList ? state.userDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setUserData(userData) {
    set({ userData });
  },
  removeUserData(userData) {
    const key = userData.key,
      item = userData.item;
    set((state) => ({
      userDataList: {
        ...state.userDataList,
        [key]: Collection.remove(
          state.userDataList ? state.userDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setUserDataPage(userDataPage) {
    const key = userDataPage.key,
      item = userDataPage.item;
    set((state) => ({
      userDataPage: {
        ...state.userDataPage,
        [key]: item,
      },
    }));
  },
  setUserDataEndPaging(userDataEndPaging) {
    const key = userDataEndPaging.key,
      item = userDataEndPaging.item;
    set((state) => ({
      userDataEndPaging: {
        ...state.userDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetUserDataPage(userDataPage) {
    const key = userDataPage.key;
    set((state) => ({
      userDataPage: {
        ...state.userDataPage,
        [key]: null,
      },
      userDataEndPaging: {
        ...state.userDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearUserDataList(userDataList) {
    const key = userDataList.key;
    set((state) => ({
      userDataList: {
        ...state.userDataList,
        [key]: null,
      },
    }));
  },
  clearUserDataState() {
    set({ ...initialState });
  },
});
export const useUserDataStore = create(storeFn);
