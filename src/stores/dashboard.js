import { create } from 'zustand';
import { persist } from 'zustand/middleware';

const initialState = {
  mainNav: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setMainNav(mainNav) {
    set({ mainNav });
  },
  clearDashboardState() {
    set({ ...initialState });
  },
});
export const useDashboardStore = create(
  persist(storeFn, { name: 'dashboard-storage' })
);
