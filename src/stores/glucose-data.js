import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  glucoseData: null,
  glucoseDataList: null,
  glucoseDataPage: null,
  glucoseDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listGlucoseDataList(glucoseDataList) {
    const key = glucoseDataList.key,
      items = glucoseDataList.items;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: Collection.list(
          state.glucoseDataList ? state.glucoseDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setGlucoseData(glucoseData) {
    set({ glucoseData });
  },
  removeGlucoseData(glucoseData) {
    const key = glucoseData.key,
      item = glucoseData.item;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: Collection.remove(
          state.glucoseDataList ? state.glucoseDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setGlucoseDataPage(glucoseDataPage) {
    const key = glucoseDataPage.key,
      item = glucoseDataPage.item;
    set((state) => ({
      glucoseDataPage: {
        ...state.glucoseDataPage,
        [key]: item,
      },
    }));
  },
  setGlucoseDataEndPaging(glucoseDataEndPaging) {
    const key = glucoseDataEndPaging.key,
      item = glucoseDataEndPaging.item;
    set((state) => ({
      glucoseDataEndPaging: {
        ...state.glucoseDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetGlucoseDataPage(glucoseDataPage) {
    const key = glucoseDataPage.key;
    set((state) => ({
      glucoseDataPage: {
        ...state.glucoseDataPage,
        [key]: null,
      },
      glucoseDataEndPaging: {
        ...state.glucoseDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearGlucoseDataList(glucoseDataList) {
    const key = glucoseDataList.key;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: null,
      },
    }));
  },
  clearGlucoseDataState() {
    set({ ...initialState });
  },
});
export const useGlucoseDataStore = create(storeFn);
